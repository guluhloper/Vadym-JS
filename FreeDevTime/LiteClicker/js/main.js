
document.addEventListener("DOMContentLoaded", function() {
    
    let game, playerX, hit, bite, activeSkill, foe, lastPlayer, topList, localTopList;
    let skillDmg = 0;
    let startForm = document.forms.startForm;
    let mainWind = document.getElementById('main');

    let sidebar = document.getElementById('sidebar');
    sidebar.children[0].addEventListener('click', startGame)
    sidebar.children[1].addEventListener('click', endGame)
    sidebar.children[2].addEventListener('click', function(){location.reload()})

    let hero = document.getElementById('playerBlock');
    let heroHP = hero.querySelector("#hp");
    let heroSP = hero.querySelector("#sp");
    let heroSkill = hero.querySelector("#skill");
    let heroHeal = hero.querySelector("#heal");
    let heroImg = hero.querySelector("img");
    let heroPower = 0;

    let enemy = document.getElementById('foeBlock');
    let enemyHP = enemy.querySelector("#hp");
    let enemySP = enemy.querySelector("#sp");
    let enemyImg = enemy.querySelector("img");
    let enemyPower = 0;

    let log = document.getElementById('log');
    let log1 = document.createElement('p')
    let log2 = document.createElement('p')
    let log3 = document.createElement('p')

        log.appendChild(log1)
        log.appendChild(log2)
        log.appendChild(log3)

    let gg = document.querySelector(".gg");

    function player (name,char) { //конструктор игрока
        this.name = name,
        this.char = char
    }

    function newPlayer (event) {
        event.preventDefault();
        playerX = new player(startForm.charName.value, startForm.charClass.value)
        if (playerX.char === 'Sword') {
            playerX.atk = 290;
            playerX.hp = 200;
            playerX.sp = 10;
            playerX.maxSP = 100;
            playerX.castSP = 50;
            activeSkill = () => { // активное умение
                if(playerX.sp >= 50){
                    skillDmg = (100 + playerX.hp * game.lvl)
                    foe.hp -= skillDmg
                    playerX.sp -= 50
                }
            }
            playerX.img = 'files/p-1.png'
            heroSkill.style.backgroundPosition = '0 0'
        } else if (playerX.char === 'Rogue') {
            playerX.atk = 39;
            playerX.hp = 130;
            playerX.sp = 10;
            playerX.maxSP = 75;
            playerX.castSP = 25;
            activeSkill = () => {
                if(playerX.sp >= 25){
                    skillDmg = (Math.floor(Math.random() * ((foe.hp / 2) - 10))) + 10
                    foe.hp -= skillDmg
                    playerX.sp -= 25
                }
            }
            playerX.img = 'files/p-2.png'
            heroSkill.style.backgroundPosition = '-75px 0px'
        } else if (playerX.char === 'Mage') {
            playerX.atk = 18 ;
            playerX.hp = 100;
            playerX.sp = 10;
            playerX.maxSP = 200;
            playerX.castSP = 25;
            activeSkill = () => {
                if(playerX.sp >= 25){
                    skillDmg = (playerX.sp * foe.lvl * game.stage)
                    foe.hp -= skillDmg
                    playerX.sp -= (Math.floor(playerX.sp / 1.33))
                }
            }
            playerX.img = 'files/p-3.png'
            heroSkill.style.backgroundPosition = '-76px -74px'
        }

        if (startForm.charName.value === "") { // если нет имени персонажа
            playerX.name = "Странник"
            hero.querySelector('#name').innerHTML = playerX.name;
        } else {
            hero.querySelector('#name').innerHTML = playerX.name;
        }

        heroImg.src =  playerX.img; // аватарка героя

        heroHP.setAttribute('data-hp', playerX.hp) // записываем параметры в дата атребуты
        heroSP.setAttribute('data-sp', playerX.maxSP)

        startForm.style.display = 'none'; //прячем первое окно
        document.getElementById('game').classList.toggle('play'); //показывает основное окно
    }

    startForm.addEventListener('submit', newPlayer); //подтверждение формы

    function stats() { // выводим числовые значения HP и SP 
        
        enemyHP.setAttribute('data-hp', foe.hp)
        enemyHP.style.width = '100%'
        enemySP.setAttribute('data-sp', foe.maxSP)
        enemySP.style.width = 0;
        enemyImg.src = 'files/lvl-' + game.stage + '-' + foe.lvl + '.png'

        heroHP.innerHTML = playerX.hp
        heroSP.innerHTML = playerX.sp
        enemyHP.innerHTML = foe.hp
        enemySP.innerHTML = foe.sp

        hit  = 'БЕЙ!!!';
        bite = 'готовится ударить!!!';
        skillDmg = skillDmg;
    }
    
    function hpBar (elemHP, perent) { // вычисляем полоску HP
        elemHP.style.width = (perent.hp / elemHP.dataset.hp) * 100 + "%"
        elemHP.innerHTML = perent.hp
    }

    function logAtk () { //логи урона
        if (game.lvl < 15) {
            log1.innerHTML = 'Удар по монстру: ' + hit
            log2.innerHTML = 'Удар по герою: ' + bite
            log3.innerHTML = 'Удар навыком: ' + skillDmg
        } else {
            log1.innerHTML = '';
            log2.innerHTML = '';
            log3.innerHTML = '';
        }
        
    }

    function heroAtk () { // накопление SP героем, полока SP, активация умения если SP больше макс SP параметра
        playerX.sp += (Math.floor(Math.random() * 2) + 1)
        heroSP.style.width = (playerX.sp / playerX.maxSP) * 100 + "%"
        
        if (playerX.sp >= playerX.maxSP) { // SP не больше максимального
            playerX.sp = playerX.maxSP 
            heroSP.style.width = '100%'
        }

        if (playerX.sp >= playerX.castSP) { //активация иконки умения при до достаточном количестве силы
            heroSkill.style.opacity = '1'
        } else {
            heroSkill.style.opacity = '0.2'
        }

        heroSP.innerHTML = playerX.sp
    }
    function enemyAtk () { // накопление SP врагом, полоcка SP, удар по герою когда у врагая заполнилась полоска силы
        foe.sp += (Math.floor(Math.random() * foe.lvl * 2 + 1))
        enemySP.style.width = (foe.sp / foe.maxSP) * 100 + "%"
        if (foe.sp >= foe.maxSP) {
            bite = (Math.floor(Math.random() * (foe.lvl * game.stage * 2.4) + 1)) //сила удара врага
            playerX.hp -= bite
            foe.sp = 0
            enemySP.style.width = 0
            hpBar(heroHP, playerX)
            logAtk()
        }
        if (playerX.hp <= 0) {
            playerX.hp = 0
            game.start = false
            hpBar(heroHP, playerX) //функция расчёты полоски здоровья
            endGame()
        }
        
        heroHP.innerHTML = playerX.hp
        enemySP.innerHTML = foe.sp
    }
        
    function nextLvl () { //новый уровень после убийства монстра
        game.lvl++;
        foe.maxSP += (4 * foe.lvl * game.stage); // чем выше уровень - тем больше силы надо набрать монстру
        foe.sp = 0;
        enemySP.style.width = 0
        if (game.lvl <= 4) { //усиления в зависимости от уровня
            game.stage = 1
            foe.lvl++
            foe.hp = 118*game.lvl
        } else if (game.lvl <= 10) { 
            game.stage = 2
            foe.lvl++
            foe.hp = 164*game.lvl
            mainWind.style.backgroundImage = 'url(./files/dung-2.jpg)' //смена сцены
        } else if (game.lvl <= 14) { 
            game.stage = 3
            foe.lvl++
            foe.hp = 216*game.lvl
            mainWind.style.backgroundImage = 'url(./files/dung-3.jpg)'
        } else if (game.lvl = 15) { //секретный уровень (не успел закончить)
            game.stage = 4
            foe.lvl++
            foe.lvl = 15
            foe.hp = 999999
            enemy.querySelector('#name').innerHTML = "Моника";
            mainWind.style.backgroundImage = 'url(./files/dung-4.jpg)'

            monika1 = setTimeout(function(){ log1.innerHTML = 'Добро пожаловать, ' + playerX.name + '.'}, 1000)
            monika2 = setTimeout(function(){ log1.innerHTML = 'Присаживайся. Съешь ещё этих мягких французских булок, да выпей же чаю.'}, 5000)
            monika3 = setTimeout(function(){ log1.innerHTML = 'Приходи снова. Я буду жать тебя тут...'}, 14000)
            monika3 = setTimeout(function(){ log1.innerHTML = 'всегда..'}, 17000)
        }

        playerX.sp += 10
        stats()
    }

    heroHeal.addEventListener('click', function(){ // лечение персонажа, один раз
        if ((playerX.hp + 50) >= heroHP.dataset.hp) {
            playerX.hp = heroHP.dataset.hp
        } else {
            playerX.hp += 50
        }
        hpBar(heroHP, playerX)
        heroHeal.style.pointerEvents = 'none';
        heroHeal.style.opacity = '0.1'
    })

    heroSkill.addEventListener('click', function(){ //применение умения героя
        activeSkill()
        if (foe.hp <= 0) {
            nextLvl()
        }            
        hpBar(enemyHP, foe)
        logAtk()
    })

    enemy.addEventListener('click', function() { //атака по монстру кликом мышки
        hit = (Math.floor(Math.random() * (playerX.atk * 2) + 10)) // разброс урона
        if (foe.hp - hit <= 0) {
            nextLvl()
        } else {
            foe.hp -= hit
            hpBar(enemyHP, foe)
        }
        logAtk()
    })

    function startGame () { //старт игры, сброс значений
        game = {
            lvl: 1,
            stage: 1
        }
    
        foe = {
            lvl: 1,
            hp: 90,
            sp: 0,
            maxSP: 20
        }

        stats()
        
        heroPower = setInterval(heroAtk, 300) //накопление маны, проверка готовности умения
        enemyPower = setInterval(enemyAtk, 300) //накопление силы монстром

        sidebar.children[0].setAttribute("disabled", true); //блокировка кнопки старт

        //Подготовка к старту игры
        playerX.hp = heroHP.dataset.hp; //возвращаем полное здоровье из дата атребута
        playerX.sp = 10;
        heroHP.style.width = '100%'

        mainWind.style.opacity = 1;
        mainWind.style.pointerEvents = 'all'
        hero.style.opacity = 1;
        enemy.style.opacity = 1;
        heroHeal.style.opacity = 1
        heroHeal.style.pointerEvents = 'all';
        gg.style.display = 'none';

        console.log('Начало игры')
    }

    function endGame () { //конец игры

        sidebar.children[0].removeAttribute("disabled");

        clearInterval(heroPower) //остановка просчёта
        clearInterval(enemyPower)

        mainWind.style.opacity = 0.5;
        mainWind.style.pointerEvents = 'none'
        gg.style.display = 'block'; //окно конца игры

        console.log('Конец игры')
    }

    
});
