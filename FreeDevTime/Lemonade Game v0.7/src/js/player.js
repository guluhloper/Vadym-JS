import _Store from "./data-store"
import modals from "./modal/modals"
class Player {
    constructor(name, money, lemons, water, sugar){
        this.name = name;
        this.money = money;
        this.lemons = lemons;
        this.water = water;
        this.sugar = sugar;
    }
}
let newPlayer = () => { 
    let form = document.getElementById('submitForm')
        form.addEventListener('click', () => {
            let playerName = document.querySelector('input[name="name"]').value;
            let dif        = document.querySelector('input[name="dif"]:checked').value;
            let el = 
            dif == 'easy' ? new Player(playerName, 3000, 10, 10, 1000) :
            dif == 'hard' ? new Player(playerName, 200, 0, 0, 0) : 'Mode Error';

            modals.modalNoOff('new-game') //спрятали форму
            document.querySelector('.new-game').setAttribute('style','z-index: 0') //на задний план
            
            _Store.setStore(el); //обновили склад
        })
}
newPlayer()

export default {Player, newPlayer};