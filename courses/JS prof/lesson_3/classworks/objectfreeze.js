/*
  Задание: написать функцию, для глубокой заморозки обьектов.

  Обьект для работы:
  let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    }
  };

  frozenUniverse.evil.humans = []; -> Не должен отработать.

  Методы для работы:
  1. Object.getOwnPropertyNames(obj);
      -> Получаем имена свойств из объекта obj в виде массива

  2. Проверка через typeof на обьект или !== null
  if (typeof prop == 'object' && prop !== null){...}

  Тестирование:

  let FarGalaxy = DeepFreeze(universe);
      FarGalaxy.good.push('javascript'); // false
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

*/

const work = () => {
  let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    },
    obj: {
        obj: {
          obj: {
            cat: 'frozen'
          }
        }
    }
  };

  // let frozen = Object.freeze(universe);
  // console.log(frozen)

  // let objArr = Object.getOwnPropertyNames(universe);
  // console.log(objArr)

  // let DeepFreeze = Object.freeze(objArr);
  // console.log(DeepFreeze)

  const DeepFreeze = obj => {
    let objArr = Object.getOwnPropertyNames(obj);
    if (typeof obj == 'object' && obj !== null) {
      objArr.forEach( prop => {
        if( typeof obj[prop] === "object"  && obj[prop] !== null){
          DeepFreeze( obj[prop] );
        };
      });
    };
    return Object.freeze( obj ); 
  };


  // for( let key in obj ){
  //   console.log( key, obj, obj[key])
  // }

  let FarGalaxy = DeepFreeze(universe);
  console.log()
  console.log('Заморожено: ' + Object.isFrozen(FarGalaxy))
  console.log('Заморожено: ' + Object.isFrozen(FarGalaxy.evil))
  console.log('Заморожено: ' + Object.isFrozen(FarGalaxy.evil.bonuses))
  // FarGalaxy.good.push('javascript'); // false
  // FarGalaxy.something = 'Wow!'; // false
  // FarGalaxy.evil.humans = [];   // false
}

export default work;
