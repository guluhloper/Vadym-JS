/*
  Задание:

  Написать класс SuperDude который как аргумент принимает два параметра:
    - Имя
    - Массив суперспособностей которые являются обьектом.

    Модель суперспособности:
      {
        // Имя способности
        name:'Invisibility',
        // Сообщение которое будет выведено когда способность была вызвана
        spell: function(){ return `${this.name} hide from you`}
      }

    В конструкторе, нужно:
    - сделать так, что бы имя нельзя было перезаписывать и присвоить ему то
      значение которое мы передали как аргумент.

    - перебрать массив способностей и на каждую из них создать метод для этого
      обьекта, используя поле name как название метода, а spell как то,
      что нужно вернуть в console.log при вызове этого метода.
    - все способности должны быть неизменяемые

    - бонус, создать конструктор суперспособностей -> new Spell( name, spellFunc );
*/

  class SuperDude {
    constructor(name, spells){
      this.name = name,
      spells.forEach( action => {
          Object.defineProperty( this, action.name, {
            value: action.spell,
            configurable: false, writable: true, enumerable: true
          });
      })
    }
  }

  let superPowers = [
    { name:'Invisibility', spell: function(){ return `${this.name} hide from you`} },
    { name:'superSpeed', spell: function(){ return `${this.name} running from you`} },
    { name:'superSight', spell: function(){ return `${this.name} see you`} },
    { name:'superFroze', spell: function(){ return `${this.name} will froze you`} },
    { name:'superSkin',  spell: function(){ return `${this.name} skin is unbreakable`} },
  ];

  class Spell {
    constructor(name, spellFunc) {
      this.name = name;
      this.spell = function() {
        return this.name + ' ' + spellFunc;
      };
    }
  }


  let superPower1 = new Spell( 'superPuper', 'super powerful' );
  superPowers.push(superPower1);


  

  // Object.defineProperty(superPowers, 'name', {
  //   configurable: false, writable: true, enumerable: true
  // });
  // Object.defineProperty(superPowers, 'spell', {
  //   configurable: false, writable: false, enumerable: true
  // });

  let Luther = new SuperDude('Luther', superPowers);
      // Тестирование: Методы должны работать и выводить сообщение.
      console.log(
        Luther.superSight(),
        // Luther.superSpeed()
        // Luther.superFroze()
        // Luther.Invisibility()
        // Luther.superSkin()
        Luther.superPuper()
      );


