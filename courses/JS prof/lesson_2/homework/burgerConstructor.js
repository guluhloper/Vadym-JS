

  /*

    Задание:

      1. Создать конструктор бургеров на прототипах, которая добавляет наш бургер в массив меню

      Дожно выглядеть так:
      new burger('Hamburger',[ ...Массив с ингредиентами ] , 20);

      Моделька для бургера:
      {
        cookingTime: 0,     // Время на готовку
        showComposition: function(){
          let {composition, name} = this;
          let compositionLength = composition.length;
          console.log(compositionLength);
          if( compositionLength !== 0){
            composition.map( function( item ){
                console.log( 'Состав бургера', name, item );
            })
          }
        }
      }

      Результатом конструктора нужно вывести массив меню c добавленными элементами.
      // menu: [ {name: "", composition: [], cookingTime:""},  {name: "", composition: [], cookingTime:""}]

        2. Создать конструктор заказов

        Моделька:
        {
          id: "",
          orderNumber: "",
          orderBurger: "",
          orderException: "",
          orderAvailability: ""
        }

          Заказ может быть 3х типов:
            1. В котором указано название бургера
              new Order('Hamburger'); -> Order 1. Бургер Hamburger, будет готов через 10 минут.
            2. В котором указано что должно быть в бургере, ищете в массиве Menu подходящий вариант
              new Order('', 'has', 'Название ингредиента') -> Order 2. Бургер Чизбургер, с сыром, будет готов через 5 минут.
            3. В котором указано чего не должно быть
              new Order('', 'except', 'Название ингредиента') ->


            Каждый из которых должен вернуть статус:
            "Заказ 1: Бургер ${Название}, будет готов через ${Время}

            Если бургера по условиям заказа не найдено предлагать случайный бургер из меню:
              new Order('', 'except', 'Булка') -> К сожалению, такого бургера у нас нет, можете попробовать "Чизбургер"
              Можно например спрашивать через Confirm подходит ли такой вариант, если да - оформлять заказ
              Если нет, предложить еще вариант из меню.

        3. Протестировать программу.
          1. Вначале добавляем наши бургеры в меню (3-4 шт);
          2. Проверяем работу прототипного наследования вызывая метод showComposition на обьект бургера с меню
          3. Формируем 3 заказа

        Бонусные задания:
        4. Добавлять в исключения\пожелания можно несколько ингредиентов
        5. MEGABONUS
          Сделать графическую оболочку для программы.

  */

  const Ingredients = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Бекон',
    'Рыбная котлета',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Виолла',
    'Сыр Гауда',
    'Майонез'
  ];

  var OurMenu = [];
  var OurOrders = [];
  var orderNum = 0;

let burgerModel = {
  cookingTime: 0,     // Время на готовку
  showComposition: function(){
    let {composition, name} = this;
    let compositionLength = composition.length;
    console.log(compositionLength);
    if( compositionLength !== 0){
      composition.map( function( item ){
          console.log( 'Состав бургера', name, item );
      })
    }
  }
}

function Burger (name, composition, cookingTime) {
  this.name = name;
  this.composition = [ 'Булка', ...composition];
  this.cookingTime = cookingTime;
  OurMenu.push({name, composition, cookingTime})
}
Object.setPrototypeOf(Burger, burgerModel);

let burger_1 = new Burger('Chees Burger',       ['Котлетка','Сыр Чеддер', 'Майонез','Огурчик','Помидорка'],                           5 );
let burger_2 = new Burger('Chees Becon Burger', ['Котлетка','Бекон','Сыр Чеддер','Майонез','Огурчик','Помидорка'],                    7 );
let burger_3 = new Burger('Fish Burger',        ['Рыбная котлета','Маслины','Сыр Виолла','Майонез','Кисло-сладкий соус','Помидорка'], 8 );
let burger_4 = new Burger('Chill Burger',       ['Котлетка','Бекон','Сыр Чеддер','Майонез','Огурчик','Помидорка','Острый перец'],     10 );

function Order(name, condition, value){

  let burgerByName     =  OurMenu.find(el => el.name == name);
  let someIngredient   =  OurMenu.find(el => el.composition.some(el => el == value));
  let exceptIngredient =  OurMenu.find(el => el.composition.some(el => el !== value));
  orderNum += 1;

  if (burgerByName !== undefined && name == burgerByName.name) {
    name = burgerByName.name;
    condition = '';
    ingredient = burgerByName.composition;
    cookingTime = this.cookingTime;
    OurOrders.push({name, condition, value});
  } else if (someIngredient !== undefined && condition == 'has') {
    name = someIngredient.name;
    cookingTime = someIngredient.cookingTime;
    OurOrders.push({name:someIngredient.name, condition, value});
  } else if (exceptIngredient !== undefined && condition == 'except') {
    name = exceptIngredient.name;
    cookingTime = exceptIngredient.cookingTime;
    OurOrders.push({name:exceptIngredient.name, condition, value});
  } else {
    let randomBurger = OurMenu[Math.floor(Math.random()*OurMenu.length)];
    console.log(`В меню нет такого варианта. Вы можете попробовать: ${randomBurger.name}, он готовится ${randomBurger.cookingTime} минут.`);
  }
  if (OurOrders.length == orderNum) {
    console.log(`Заказ ${orderNum} : ${name}, будет готов через ${cookingTime} минут.`)
  } 
}

let order1 = new Order('Chees Burger');
let order2 = new Order('Fish Burger');
let order3 = new Order('', 'has', 'Острый перец');
let order4 = new Order('', 'except', 'Кисло-сладкий соус');
let order5 = new Order('', 'except', 'Сыр Чеддер');
let order6 = new Order('', 'has', 'Салями');

