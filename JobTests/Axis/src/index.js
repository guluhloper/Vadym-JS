import "./style.scss";
let pageName = 'cassa'
let currency = '$'
let cartCurrency = '€'
let products = [
    {id: 1, name: 'pinapple', price: 19.00, newPrice: 8.50, count: 1, img: 'product-1.png'},
    {id: 2, name: 'berries', price: 19.00, newPrice: 8.50, count: 1, img: 'product-2.png'},
    {id: 3, name: 'grapefruit', price: 19.00, newPrice: 8.50, count: 1, img: 'product-3.png'},
    {id: 4, name: 'apple', price: 19.00, newPrice: 8.50, count: 1, img: 'product-4.png'}
]

let cart = []

let renderProduct = (i) => {
    let {id, name, price, newPrice, count, img} = products[i]
    let p1 = 0
    let p2 = 0
    if(newPrice > 0){
        p1 = newPrice.toFixed(2)
        p2 = price.toFixed(2)
    } else {
        p1 = price.toFixed(2)
        p2 = ''
    }
    let product = 
    `
    <div id="product_${id}" class="product">
        <div class="name">${name}</div>
        <div class="image">
            <img src="./img/products/${img}" alt="${name}">
        </div>
        <div class="info">
            <div class="promo-name">offerta limatata -50%</div>
            <div class="price">${currency+p1}</div>
            <div class="old-rice">${p2+currency}</div>
            <div class="buy-block">
                <div class="to-cart">
                    <div class="count">
                        <div class="num">${count}</div>
                        <div class="arrow">▼</div>
                    </div>
                    <div class="numbers">
                        <div class="scroll">
                        </div>
                    </div>
                    <div class="add-to-cart">
                        <img src="./img/buy.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
    return product;
}

let renderProducts = () => {
    let catalog = document.querySelector('.catalog')

    catalog.innerHTML = `<h2>${pageName}</h2>`

    for (let i = 0; i < products.length; i++) {
        const el = products[i];

        catalog.insertAdjacentHTML('beforeend', renderProduct(i))

        let count = document.querySelector(`#product_${el.id} .count`)
        let numberBlock = document.querySelector(`#product_${el.id} .numbers`)

        for (let i = 1; i <= 50; i++) {
            numberBlock.querySelector('.scroll').insertAdjacentHTML('beforeend', `<div>${i}</div>`)
        }

        let numbers = document.querySelectorAll(`#product_${el.id} .numbers .scroll div`)

        count.addEventListener('click', ()=>{
            numberBlock.classList.toggle('show')
            numberBlock.classList.toggle('fade-in')
        })

        numbers.forEach(num => {
            num.addEventListener('click', ()=>{               
                el.count = num.innerHTML
                renderProducts()
            })
        });

        let toCart = document.querySelector(`#product_${el.id} .add-to-cart`)
        let cartBlock = document.querySelector(`#cartPopup`)

        let addToCart = () => {
            let cartItem = cart.find(item => item.id == el.id)
            if(cartItem !== undefined){
                cartItem.count = count.querySelector('.num').innerHTML
            } else {
                cart.push(el)
            }
            renderCart()
        }

        let toggleCart = () => {
            cartBlock.classList.add('show')
            cartBlock.classList.add('fade-in')
            document.querySelector('body').setAttribute('style', 'overflow-y: hidden')
        }
        toCart.addEventListener('click', ()=>{
            addToCart()
            toggleCart()
        }, false)
    }
}

let renderCartProduct = (i) => {
    let {id, name, price, newPrice, count, img} = cart[i]
    let p1 = 0
    let p2 = 0
    let totalPrice = 0
    if(newPrice > 0){
        p1 = newPrice
        p2 = price
    } else {
        p1 = price
        p2 = ''
    }
    totalPrice = (p1 * count).toFixed(2)

    let product = `
    <div id="cart_product_${id}" class="product">
        <div class="image">
            <img src="./img/products/${img}" alt="${name}">
        </div>
        <div class="info">
            <div class="name">
            offerta 
            <br>
            fizzyslim con sapore di
            <br>
            ${name}
            </div>
            <div class="qta">
                <span>QTA:</span>
                <span>${count}</span>
            </div>
            <div class="price">
                <div class="currency">${cartCurrency}</div>
                <div class="price">${totalPrice}</div>
            </div>
        </div>
    </div>
    `
    return product;
}

let renderCart = () => {
    let products = document.querySelector('.cart .products')
    let subtotal = document.querySelector('.total .price')
    let total = 0
    products.innerHTML = ''
    subtotal.innerHTML = ''
    for (let i = 0; i < cart.length; i++) {
        const el = cart[i];
        products.insertAdjacentHTML('beforeend', renderCartProduct(i))
        let p1 = 0
        let p2 = 0
        let totalPrice = 0
        if(el.newPrice > 0){
            p1 = el.newPrice
            p2 = el.price
        } else {
            p1 = el.price
            p2 = ''
        }
        totalPrice = Number(p1 * el.count)
        total += totalPrice
    }
    subtotal.innerHTML = total.toFixed(2)

    let hideCart = (target) => {
        target.classList.remove('show')
        target.classList.remove('fade-in')
        document.querySelector('body').setAttribute('style', 'overflow-y: auto')
    }

    let cartBlock = document.querySelector(`#cartPopup`)
    let cartHeader = cartBlock.querySelector('.header')
    cartBlock.addEventListener('click', () =>{
        if(!event.target.closest('.cart')) {
            hideCart(cartBlock)
        }
    })
    cartHeader.addEventListener('click', () =>{
        hideCart(cartBlock)
    })
}

renderProducts()
renderCart()