// 1 , 2 , 3
var btns = document.getElementsByClassName('showButton');
var tabs = document.getElementsByClassName("tab");
for (var i = 0; i < btns.length; i++) {
  btns[i].setAttribute('onclick','openImg' + "('" + "tab-" + i + "')");
  tabs[i].setAttribute('id','tab-' + i);
}
function openImg(tabName) {
  for (var i = 0; i < tabs.length; i++) {
    tabs[i].classList.remove('active');
    }
  document.getElementById(tabName).classList.toggle('active');
}

// 4
/* var closeAll = document.createElement('button');
closeAll.classList.add('showButton');
closeAll.innerText = 'X';
document.getElementById('buttonContainer').appendChild(closeAll); */

/* var closeAll = document.getElementById('tabContainer'); */

function hideAllTabs () {
  for (var i = 0; i < tabs.length; i++) {
    tabs[i].classList.remove('active');
  }
}

tabContainer.addEventListener('click', hideAllTabs);



  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
