class Visitor  {
    constructor(lemonLvl, sugarLvl, waitTime, money) {
        this.id = Math.floor(Date.now())
        this.asset = () => {
            let i = Math.floor(Math.random()*41);
            return ('/img/assets/visitors/' + i + '.png');
        }
        this.sugarLvl = (Math.random()*(35-15) + 15) + sugarLvl*3
        this.lemonLvl = (Math.random()*(0.35 - 0.25) + 0.5) + lemonLvl*0.05
        this.waitTime =  Math.floor(Math.random()*10000) + 5000 + (waitTime * 1000)
        this.money =     Math.floor(Math.random()*10-1) + 1 + money
        this.model =    `<div class="visitor" id=${this.id}><div class="direction" style="background: no-repeat url(${this.asset()})"></div></div>`
    }
}

export default Visitor