/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classworks_task1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classworks/task1 */ \"./classworks/task1.js\");\n// import DecoratorExample from './DecoratorExample';\n// import DecoratorBase from './decorator';\n// import {FunctionDecorator, es7Decorator} from './es7_functional_decorator';\n\n/*\r\n  Демо декоратора\r\n*/\n// DecoratorBase();\n// DecoratorExample();\n// FunctionDecorator();\n// es7Decorator();\n\nObject(_classworks_task1__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classworks/task1.js":
/*!*****************************!*\
  !*** ./classworks/task1.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\r\n\r\n  Задание: используя паттерн декоратор, модифицировать класс Human из примера basicUsage.\r\n\r\n  0.  Создать новый конструктор, который будет принимать в себя человека как аргумент,\r\n      и будем добавлять ему массив обьектов coolers (охладители), а него внести обьекты\r\n      например мороженное, вода, сок и т.д в виде: {name: 'icecream', temperatureCoolRate: -5}\r\n\r\n  1.  Расширить обработку функции ChangeTemperature в прототипе human таким образом,\r\n      что если темпаретура становится выше 30 градусов то мы берем обьект из массива coolers\r\n      и \"охлаждаем\" человека на ту температуру которая там указана.\r\n\r\n      Обработку старого события если температура уходит вниз поставить с условием, что температура ниже нуля.\r\n      Если температура превышает 50 градусов, выводить сообщение error -> \"{Human.name} зажарился на солнце :(\"\r\n\r\n  2.  Бонус: добавить в наш прототип нашего нового класса метод .addCooler(), который\r\n      будет добавлять \"охладитель\" в наш обьект. Сделать валидацию что бы через этот метод\r\n      нельзя было прокинуть обьект в котором отсутствует поля name и temperatureCoolRate.\r\n      Выводить сообщение с ошибкой про это.\r\n\r\n*/\nconst BeachParty = () => {\n  class Human {\n    constructor(name) {\n      this.name = name;\n      this.temperature = 0;\n      this.coolers = [{\n        name: 'water',\n        temperatureCoolRate: -2\n      }, {\n        name: 'icecream',\n        temperatureCoolRate: -5\n      }, {\n        name: 'beer',\n        temperatureCoolRate: -8\n      }, {\n        name: 'icebucket',\n        temperatureCoolRate: -17\n      }];\n    }\n\n    ChangeTemperature(value) {\n      let temperatureWas = this.temperature;\n      this.temperature = temperatureWas - value;\n    }\n\n  }\n\n  class Cooling {\n    thisMayHelp() {}\n\n  }\n\n  console.log('your code ');\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (BeachParty);\n\n//# sourceURL=webpack:///./classworks/task1.js?");

/***/ })

/******/ });