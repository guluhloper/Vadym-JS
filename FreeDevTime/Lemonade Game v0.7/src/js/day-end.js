
import _Store from './data-store'
import modal from './modal/modals'
import {newLemonade, renderLemonades} from './render/lemonade-mashine'

let daySum = {
    update(day, visitors ,price ,sold){
        modal.modalNoOff('day-end')
        let block = modal.dayEnd()
        block.querySelector('h2').innerHTML = `День ${day} закончился`
        block.querySelector('.summary').innerHTML = `
            <p>Посетителей:</p> <span>${visitors.length}</span>
            <p>Продано:</p>     <span>${sold}</span>
            <p>Прибыль:</p>     <span>${price*sold}</span>
        `
        _Store.clearShelf()
        renderLemonades()
    }
}
export default daySum