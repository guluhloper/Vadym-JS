export { popup }
import { updateSettings, loadSettings, loadWheel, runWheel } from "./canvas"

const coolImages = require("cool-images")
const popupS = require('popups');

let popup = (num) => {
    let img = './images/placeholder.png'
    let loadedUrl = coolImages.one(800, 800)
    // Подгрузка картинки с плагина, после отображения модельного окна
    let xhr = new XMLHttpRequest();
    xhr.open('GET', loadedUrl, true)
    xhr.send();
    xhr.onload = () => {          
        if (xhr.status != 200) {
            img = '../images/placeholder.png'
        }  else {
            let image = document.querySelector('.picture img')
            image.src = loadedUrl
        }         
        
    }  

    // Вызов модельного окна
    popupS.window({
        mode: 'alert',
        additionalBaseClass: 'wheelModal',
        additionalButtonOkClass: 'start-btn',
        title: 'You win this!!!',
        labelOk: 'Try one more time!',
        content: 
        `
        <div class='modal-content'>
            <div class="picture">
                <img src="${img}" alt="Winner image!">
            </div>
            <div class="win-number">${num}</div>
        </div>
        `,
        onSubmit: function(val){ 
            runWheel()
        },
        onClose: function(){
            updateSettings()
            // Можно сбросить позицию колеса после закрытия модалки.
            // loadWheel()
        }
    });

    let popup = document.querySelector('.popupS-layer.popupS-open')
    let popupBody = popup.parentNode
    popupBody.classList.add("popup-body")
}