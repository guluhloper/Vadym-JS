import _Store from '../data-store'
import Observer from './observer'

let observer = new Observer()

let uiData = {
    updateTopBar() {
        document.querySelector('.money .topbar_number').innerHTML = _Store.getStore().money
        document.querySelector('.water .topbar_number').innerHTML = _Store.getStore().water
        document.querySelector('.lemons .topbar_number').innerHTML = _Store.getStore().lemons
        document.querySelector('.sugar .topbar_number').innerHTML = _Store.getStore().sugar
        this.showLog()
    },
    showLog() {
        let block = document.querySelector('.logs')
        block.innerHTML = `<p>${_Store.getLog()[0]}</p>`
    },
    time(time) {
        document.querySelector('.time p').innerHTML = `${time}:00`
    }
}


// UI обновление  
observer.subscribe(data => {
    let arr = ['money', 'water', 'lemons', 'sugar']
    for (let i = 0; i < arr.length; i++) {
        document.querySelector(`.${arr[i]} .topbar_number`).innerHTML = data[arr[i]]
    }
})
document.querySelector('main').addEventListener('click', () => {
    observer.broadcast(_Store.getStore())
})

export default uiData