
import _Store from "./data-store"
import Visitor from './render/units'
import uiData from "./render/ui"
import madals from "./modal/modals"
import Observer from "./render/observer"
import animation from "./render/animation";
import endDay from './day-end'


// 1*3*250
// water    1000ml
// lemonLvl 0.3
// sugarLvl 25

let startDay = {
    lemonadeQE: {},
    water: 1,
    price: 10,
    sold: 0,
    time: 0,
    timeMult: 0.5,
    day: {
        num: 0,
        wether: ['normal', 'suny', 'rain'],
        kids: false,
        wikend: 1
    },
    visitors: [],
    visitorsWating: [],
    visitorsInDom: document.querySelector('.visitors'),
    initWork() {
        document.querySelector('.start_day').addEventListener('click', () => {
            madals.modalNoOff('day-start')
            startDay.selected()
            startDay.working()
            this.day.num++
        })
    },    
    selected() {
        let findId = Number(document.querySelector('.recipe_in_store input.selected').id.split('_')[1]) + 100
        let selected = _Store.getStore().shelf.find(el => el.id == findId)
        let {lemons, water, sugar} = selected
        this.lemonadeQE =  {
            sugarLvl: (sugar/(water*1000))*100,
            lemonLvl: (lemons/(water*1000))*100
        },
        this.water = water;
        this.price = Number(document.querySelector('.set_price .num').textContent)
        _Store.lastSelectedLemonade(selected.id)
    },
    working() { // рабочий день, просчёт времени
        this.time = 9
        let {wikend} = this.day
        wikend = wikend <= 7 ? wikend+1 : 1
        let procent = 0;
        let gameTime = setInterval(() => {
            let chanse = () => { // псевдорандом
                procent += 5;
                return procent + Math.random()*(70-1)+1
            }
            if (chanse() > 90) {
                let visitor = new Visitor(1,1,1,10)
                this.move.toTent(visitor)
                procent = 0
            }
            if (this.time >= 18) { // рабочее время
                clearInterval(gameTime)
                this.time = 18
                endDay.update(this.day.num, this.visitors, this.price, this.sold) // итоги конца дня
            } else {
                this.time = this.time + 0.25
                uiData.time(Math.floor(this.time))
            }
        }, 1000/this.timeMult)
    },
    move: {
        getTrack: { // пока только один
            track: 'from-right',
            duration: 5200,
            randomTrack() { 
                let i = Math.floor(Math.random()*(0)+0)
                let tracks = ['from-right']
                let durations = [5200]
                this.track = tracks[i];
                this.duration = durations[i]
            }
        },
        toTent (visitor, from) {
            this.getTrack.randomTrack()                                           // выбрали рандомный трек начала движения
            startDay.visitorsInDom.insertAdjacentHTML('afterbegin', visitor.model) // созданого посетителя закинули в дом
            let thisVisitorInDom = document.getElementById(visitor.id)
            animation.unit(visitor.id)                                            // подключили анимацию в зависимости от направления движения
            thisVisitorInDom.classList.add(this.getTrack.track)
            if (this.tracks == 'from-right') {
            }
            let toStay = setTimeout(() => { 
                thisVisitorInDom.classList.remove(this.getTrack.track)
                this.wait(visitor, thisVisitorInDom) 
            }, this.getTrack.duration)
        },
        wait (visitor, thisVisitorInDom) {
            thisVisitorInDom.classList.add('stay')
            startDay.visitors.push(visitor)
            let waitTime = visitor.waitTime

            let waitCheck = setTimeout(() => { // проверка на покупку
                let sold = this.visitorCheck(visitor)
                if (startDay.time >= 18 || waitTime <= 0 || sold == false) { //  не продали
                    this.goOut(visitor, thisVisitorInDom)
                    _Store.storeLog('Лимонад  не купили')
                    uiData.showLog()
                } else if (startDay.time < 18 && waitTime > 0 && sold == true) { //  продали
                    _Store.storeLog('Продали лимонад за: ' + startDay.price)
                    startDay.sold++
                    this.goOut(visitor, thisVisitorInDom)
                    _Store.sellLemonade(startDay.price)
                    uiData.updateTopBar()
                    clearInterval(waitCheck)
                }
                waitTime = waitTime - (1000/startDay.timeMult) 
            }, 1000/startDay.timeMult)
        },
        goOut (visitor, thisVisitorInDom) { // ** вынести
            let randomTrack =  Math.floor(Math.random()*(0)+0)
            let tracks = ['go-out'];
            let duration = [20000]

            thisVisitorInDom.classList.remove('stay')
            thisVisitorInDom.classList.add('go-out')


            let out = setTimeout(() => {
                thisVisitorInDom.classList.remove(tracks[randomTrack])

                thisVisitorInDom.classList.add('hide')
            }, duration[randomTrack])
        },
        visitorCheck(visitor){
            let sold;
            if (   visitor.sugarLvl > startDay.lemonadeQE.sugarLvl * 0.5 && 
                   visitor.sugarLvl < startDay.lemonadeQE.sugarLvl * 1.6 &&
                   visitor.lemonLvl > startDay.lemonadeQE.lemonLvl * 0.7 && 
                   visitor.lemonLvl < startDay.lemonadeQE.lemonLvl * 1.8 &&
                   visitor.money    > startDay.price * 1 ){ 
                sold = true
            } else {
                sold = false
            }
            return sold
        },
    },
};

startDay.initWork() 

export default startDay
