import _Store from '../data-store';


class LemonBeast4000 {
    constructor(name, lemons, water, sugar){
        this.name = name == '' ? 'New lemonade' : name;
        this.lemons = lemons;
        this.water = water;
        this.sugar = sugar;
    }
}

let newLemonade = () => {
    let btn = document.querySelector('.create_lemonade'); // кнопка создать лимонад
    btn.addEventListener('click', () => {
        let recipeData = {
            // name: document.querySelector('.lemonade-name').textContent,
            lemons: Number(document.querySelector('.rs-label.num-1').textContent),
            water:  Number(document.querySelector('.rs-label.num-0').textContent),
            sugar:  Number(document.querySelector('.rs-label.num-2').textContent),
        }

        if (recipeData.lemons <= _Store.getStore().lemons && recipeData.water <= _Store.getStore().water && recipeData.sugar <= _Store.getStore().sugar) {
            let el = new LemonBeast4000(name, recipeData.lemons, recipeData.water, recipeData.sugar);           
            _Store.addLemonade(el)
            renderLemonades()
        } 
    })
}
newLemonade()

// Рендерим рецепты созданых лимонадов для модалок.

let renderLemonades = (el) => { // Где рендерить лимонады
    document.querySelectorAll('.recipes_history').forEach(element => {
        element.innerHTML = "" 
        renderOneLemonade(element)
    })    
}

let renderOneLemonade = (el) => {
    let inStore = _Store.getStore().shelf; // массив лимонадов в харилище
    for (let i = 0; i < inStore.length; i++) {
        el.insertAdjacentHTML('beforeend',`
        
        <div class="recipe_in_store">
            <input type='radio' name='lemonadeSelect' id='select_${inStore[i].id-100}'>
            <img src="/img/icons/lemonade-glas.png" alt="">
            <div class="num">${inStore[i].id-100}</div>
            <div class="recipe_detal">
                <p>Воды:   ${inStore[i].water}</p>
                <p>Лимонов:${inStore[i].lemons}</p>
                <p>Сахара: ${inStore[i].sugar}</p>
            </div>
        </div>
        `)
    }    
}




export {newLemonade, renderLemonades};
