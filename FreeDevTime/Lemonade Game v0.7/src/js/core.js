
import Area from './render/area'
import Visitor from './render/units'
import animation from "./render/animation";
import Modal from "./modal/modals"
import _Store from "./data-store"
import setPlayer from "./player"
import productShop from "./modal/product-shop"
import lemonade from "./render/lemonade-mashine"
import startDay from "./day-start"



// точка запуска
let game = { 
    run(){}
}
export default game;