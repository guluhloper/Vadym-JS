var gulp = require('gulp');

gulp.task('default', function() {
  // place code for your default task here
});

var sass = require('gulp-sass');
gulp.task('sass', function(){
  gulp.src('source-files')
    .pipe(sass()) // Using gulp-sass
    .pipe(gulp.dest('destination'))
});

gulp.task('watch', function() {
  gulp.watch('js/*.js', ['js']);
  gulp.watch('css/*.css', ['css']);
})

var connect = require('gulp-connect');

gulp.task('connect', function() {
  connect.server({
    root: '.',
    livereload: true
  })
});