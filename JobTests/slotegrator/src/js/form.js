// Errors
let required = 'Поле обязательно для заполнения';
let phoneError = 'Неверный номер телефона';
let emailError = 'Неверный Email';

(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
            let inputs = form.querySelectorAll('input')
            inputs.forEach(el => {

                if(!el.validity.valid){
                    el.classList.add('invalid-input')
                }
                
                el.addEventListener('focusin', () => {
                    el.classList.remove('invalid-input')
                    el.placeholder = ''
                })

                if(el.validity.valueMissing){
                    el.placeholder = required
                }

                if (el.validity.patternMismatch) {
                    el.value = ''
                    el.placeholder = phoneError
                }

                if (el.validity.typeMismatch) {
                    el.value = ''
                    el.placeholder = emailError
                    
                }                 
            });
          
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();