let ctx = document.getElementById('canv-2').getContext('2d')

let cW = 500
let cH = 500

let elW = 100
let elH = 100

let x = 100
let y = 100

let step = 100

let play = false
let recording = false

let newRecord = []

let catImg = new Image()
catImg.src = "../images/cat.png"
catImg.onload = function() {
    ctx.drawImage(catImg, x, y, elW, elH)  
}

let draw = () => {
    ctx.beginPath()
    ctx.rect(x, y, elW, elH)
    ctx.strokeStyle = '#9e9e9e85'
    ctx.stroke()

    drawImg()

    up.disabled = (y <= 0) ? true : false
    down.disabled = (y >= cH - elH) ? true : false
    left.disabled = (x <= 0) ? true : false
    right.disabled = (x >= cW - elW) ? true : false  
}
let drawPath = (lastX, LastY, text) => {
    ctx.beginPath()

    ctx.rect(lastX, LastY, elW, elH)
    ctx.fillStyle = '#fff';
    ctx.fill()

    ctx.strokeStyle = '#333';
    ctx.stroke()

    ctx.fillStyle = '#666';
    ctx.fillText(text, lastX + 50, LastY + 50)
}

let drawImg = () => {
    ctx.drawImage(catImg, x, y, elW, elH)
}

let record = () => {
    let name = 'cat'
    let data = {x, y}
    newRecord.push(data)
    localStorage.setItem(name, JSON.stringify(newRecord))    
}

let reset = () => {
    x = 100
    y = 100
    ctx.clearRect(0, 0, cW, cH)
    draw()
}

let moveCat = direction => { 

    if      (direction === 'up')    { y = y - step} 
    else if (direction === 'down')  { y = y + step}
    else if (direction === 'left')  { x = x - step}
    else if (direction === 'right') { x = x + step}

    ctx.clearRect(0, 0, cW, cH)
    draw()
    
    if (recording == true) { record() }
}



up.addEventListener('click', () =>    {moveCat('up')})
down.addEventListener('click', () =>  {moveCat('down')})
left.addEventListener('click', () =>  {moveCat('left')})
right.addEventListener('click', () => {moveCat('right')})

resetBtn.addEventListener('click', () => {
    reset()
})
playStopBtn.addEventListener('click', () => {
    ctx.clearRect(0, 0, cW, cH)
    recordBtn.disabled = true


    play = !play

    if(play == true) {
        playStopBtn.textContent = 'Stop'
        let recordedPath = JSON.parse(localStorage.getItem('cat'))

        let i = 0
        setTimeout(function runRecord(){
            if (i == recordedPath.length) {
                play = !play
                playStopBtn.textContent = 'Play'
                recordBtn.disabled = false
            } else if (i >= 0) {
                x = recordedPath[i].x
                y = recordedPath[i].y
                draw()
    
                if (i >= 1){
                    lastX = recordedPath[i-1].x;
                    LastY = recordedPath[i-1].y;
                    drawPath(lastX, LastY, i);
                }
                i = i + 1
                setTimeout(runRecord, 500)
            }
        }, 500)
    }
})

recordBtn.addEventListener('click', ()=>{
    recording = !recording
    recordBtn.style.color = (recording == true) ? '#e91e63' : '#000';    
    playStopBtn.disabled  = (recording == true) ? true : false;
    if (recording == true) { record() }
})

draw()