
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: true }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
            {
                loader: process.env.NODE_ENV !== 'production' ? 'style-loader' : MiniCssExtractPlugin,
            },
            {
                loader: "css-loader",
            },
            {
                loader: "resolve-url-loader",
            },
            {
                loader: "sass-loader",
                options: {
                    sourceMap: true,
                    sourceMapContents: false
                }
            }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    }),
    new MiniCssExtractPlugin({
        filename: "[name].css",
        chunkFilename: "[id].css"
      })
  ]
};