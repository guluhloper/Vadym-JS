/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classworks_objectfreeze__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classworks/objectfreeze */ \"./classworks/objectfreeze.js\");\n/* harmony import */ var _classworks_singleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../classworks/singleton */ \"./classworks/singleton.js\");\n/*\r\n\r\n  Модули в JS\r\n  https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/import\r\n\r\n  Так как для экспорта и импорта нету родной поддержки в браузерах, то\r\n  нам понадобится сборщик или транспалер который это умеет делать.\r\n  -> babel, webpack, rollup\r\n\r\n  На сегодняшний день - самое полулярное решение, это вебпак!\r\n\r\n  npm i webpack webpack-cli -D\r\n\r\n  Установка и config-less настройка\r\n\r\n  \"scripts\": {\r\n    \"cli\": \"webpack ./application/index.js --output-path ./public/js --output-filename bundle.js --mode development --color --watch\"\r\n  }\r\n\r\n*/\r\n\r\n  // import ObjectFreeze from '../classworks/objectfreeze';\r\n  // ObjectFreeze();\r\n\r\n  // `webpack\r\n  //     ./application/index.js\r\n  //     --output-path ./public/js\r\n  //     --output-filename bundle.js\r\n  //     --mode development\r\n  //     --color\r\n  //     --watch\r\n  // `;\r\n\r\n/*\r\n  npm run cli\r\n  Затестим - в консоли наберем команду webpack\r\n*/\r\n\r\n  // import imports from './imports';\r\n  // console.log('WEBPACK WORKING2!');\r\n\r\n  \r\n  \r\n\r\n  Object(_classworks_objectfreeze__WEBPACK_IMPORTED_MODULE_0__[\"default\"])()\r\n\r\n  console.log('Добавили закон: ', _classworks_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].addLaw(1,'Хороший закон.','Ну вы держитесь'));\r\n  console.log('Покажи законы: ', _classworks_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].showLaws())\r\n  console.log('Найти закон с id 1: ', _classworks_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].findLaw(1))\r\n  console.log('Казна: '+_classworks_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].showBudget())\r\n  console.log('Счастье '+_classworks_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].showLove())\r\n  console.log('Проводим праздник: '+_classworks_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].runIvent())\r\n  console.log('Казна: '+_classworks_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].showBudget())\r\n  console.log('Счастье '+_classworks_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].showLove())\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/singleton/new-singleton.js":
/*!************************************************!*\
  !*** ./application/singleton/new-singleton.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// Так как является константой, не может быть измененно\r\nconst _data = {\r\n  store: [],\r\n  counter: 0\r\n};\r\n\r\n// Создаем обьект и методы\r\nconst Store = {\r\n  add: item => _data.store.push(item),\r\n  get: id => _data.store.find( d => d.id === id ),\r\n  showAllLang: () => [ ..._data.store ],\r\n  getCounter: () => _data.counter,\r\n  addCounter: (num) => _data.counter += num\r\n};\r\n// Замораживаем\r\nObject.freeze(Store);\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Store);\r\n\n\n//# sourceURL=webpack:///./application/singleton/new-singleton.js?");

/***/ }),

/***/ "./classworks/objectfreeze.js":
/*!************************************!*\
  !*** ./classworks/objectfreeze.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\r\n  Задание: написать функцию, для глубокой заморозки обьектов.\r\n\r\n  Обьект для работы:\r\n  let universe = {\r\n    infinity: Infinity,\r\n    good: ['cats', 'love', 'rock-n-roll'],\r\n    evil: {\r\n      bonuses: ['cookies', 'great look']\r\n    }\r\n  };\r\n\r\n  frozenUniverse.evil.humans = []; -> Не должен отработать.\r\n\r\n  Методы для работы:\r\n  1. Object.getOwnPropertyNames(obj);\r\n      -> Получаем имена свойств из объекта obj в виде массива\r\n\r\n  2. Проверка через typeof на обьект или !== null\r\n  if (typeof prop == 'object' && prop !== null){...}\r\n\r\n  Тестирование:\r\n\r\n  let FarGalaxy = DeepFreeze(universe);\r\n      FarGalaxy.good.push('javascript'); // false\r\n      FarGalaxy.something = 'Wow!'; // false\r\n      FarGalaxy.evil.humans = [];   // false\r\n\r\n*/\r\n\r\nconst work = () => {\r\n  let universe = {\r\n    infinity: Infinity,\r\n    good: ['cats', 'love', 'rock-n-roll'],\r\n    evil: {\r\n      bonuses: ['cookies', 'great look']\r\n    },\r\n    obj: {\r\n        obj: {\r\n          obj: {\r\n            cat: 'frozen'\r\n          }\r\n        }\r\n    }\r\n  };\r\n\r\n  // let frozen = Object.freeze(universe);\r\n  // console.log(frozen)\r\n\r\n  // let objArr = Object.getOwnPropertyNames(universe);\r\n  // console.log(objArr)\r\n\r\n  // let DeepFreeze = Object.freeze(objArr);\r\n  // console.log(DeepFreeze)\r\n\r\n  const DeepFreeze = obj => {\r\n    let objArr = Object.getOwnPropertyNames(obj);\r\n    if (typeof obj == 'object' && obj !== null) {\r\n      objArr.forEach( prop => {\r\n        if( typeof obj[prop] === \"object\"  && obj[prop] !== null){\r\n          DeepFreeze( obj[prop] );\r\n        };\r\n      });\r\n    };\r\n    return Object.freeze( obj ); \r\n  };\r\n\r\n\r\n  // for( let key in obj ){\r\n  //   console.log( key, obj, obj[key])\r\n  // }\r\n\r\n  let FarGalaxy = DeepFreeze(universe);\r\n  console.log()\r\n  console.log('Заморожено: ' + Object.isFrozen(FarGalaxy))\r\n  console.log('Заморожено: ' + Object.isFrozen(FarGalaxy.evil))\r\n  console.log('Заморожено: ' + Object.isFrozen(FarGalaxy.evil.bonuses))\r\n  // FarGalaxy.good.push('javascript'); // false\r\n  // FarGalaxy.something = 'Wow!'; // false\r\n  // FarGalaxy.evil.humans = [];   // false\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (work);\r\n\n\n//# sourceURL=webpack:///./classworks/objectfreeze.js?");

/***/ }),

/***/ "./classworks/singleton.js":
/*!*********************************!*\
  !*** ./classworks/singleton.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _application_singleton_new_singleton__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../application/singleton/new-singleton */ \"./application/singleton/new-singleton.js\");\n\r\n\r\n/*\r\n  Задание:\r\n\r\n    Написать синглтон, который будет создавать обьект government\r\n\r\n    Данные:\r\n    {\r\n        laws: [\r\n        {\r\n          id: 0,\r\n          text: '123123'\r\n        }\r\n      ],\r\n        budget: 1000000\r\n        citizensSatisfactions: 0,\r\n    }\r\n\r\n    У этого обьекта будут методы:\r\n      .добавитьЗакон({id, name, description})\r\n        -> добавляет закон в laws и понижает удовлетворенность граждан на -10\r\n\r\n      .читатькКонституцию -> Вывести все законы на экран\r\n      .читатьЗакон(ид)\r\n\r\n      .показатьУровеньДовольства()\r\n      .показатьБюджет()\r\n      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5\r\n\r\n\r\n*/\r\n\r\nconst _data = {\r\n  laws: [],\r\n  budget: 1000000,\r\n  citizensSatisfactions: 0,\r\n}\r\n\r\nconst Government = {\r\n  addLaw: (id, name, description) => {\r\n    _data.laws.push({id, name, description});\r\n    _data.citizensSatisfactions -= 10;\r\n    return `Id: ${id}, Name: ${name}, Desc: ${description}`;\r\n  },\r\n  showLaws: () => _data.laws,\r\n  findLaw: id => _data.laws.find( d => d.id === id ),\r\n  showLove: () => _data.citizensSatisfactions,\r\n  showBudget: () => _data.budget,\r\n  runIvent: () => {\r\n    _data.budget -= 50000;\r\n    _data.citizensSatisfactions += 5;\r\n    return 'Провели праздник'\r\n  }\r\n}\r\n\r\nObject.freeze(Government);\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Government);\r\n\n\n//# sourceURL=webpack:///./classworks/singleton.js?");

/***/ })

/******/ });