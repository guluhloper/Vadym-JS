let imagesFolder = 'images'

let slider = document.querySelector('.slider')
let slide = document.getElementById('slide')
let prevSlide = document.getElementById('prevSlide')
let nextSlide = document.getElementById('nextSlide')

let slidesArr = [1, 2]
let num = 0

let loadSlide = () => {
    slide.style.backgroundImage = `url('./${imagesFolder}/slide-${slidesArr[num]}.png')`
    setTimeout(() => {
        slide.style.opacity = 1;
    }, 250);
}

let toNext = () => {
    num - 1 < 0 ? num = slidesArr.length - 1 : num = num - 1
    slide.style.opacity = 0;
    setTimeout(() => {
        loadSlide()
    }, 250);
}

let toPre = () => {
    num + 1 < slidesArr.length ? num++ : num = 0
    slide.style.opacity = 0;
    setTimeout(() => {
        loadSlide()
    }, 250);
}

prevSlide.addEventListener('click', toNext)
nextSlide.addEventListener('click', toPre)

loadSlide()

let autoSlider = setInterval( toNext, 4000)

slider.addEventListener('mouseover', () => {
    clearInterval(autoSlider)
})
slider.addEventListener('mouseout', () => {
    autoSlider = setInterval( toNext, 4000)
})