const path = require('path');
const config = require('./webpack.config');
const merge = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const CopyPlugin = require('copy-webpack-plugin');

module.exports = merge(config, {
  mode: "production",
  output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].bundle.[hash].js',
  },
  plugins: [ 
    new CleanWebpackPlugin(),
    // new CopyPlugin([
    //   { from: 'src/img/products/', to: './img/products/' }
    // ]),
  ]
})