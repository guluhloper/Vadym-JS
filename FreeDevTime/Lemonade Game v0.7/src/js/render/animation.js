

let animation = {
    unit(id){ // амнимация посетителя
        let unitBlock = document.getElementById(id)
        let unitAsset = unitBlock.querySelector('div')
        
        let direction = () => { //направление анимации в направлении движения 
            let x = unitBlock.offsetLeft
            let y = unitBlock.offsetTop
            let goX = (go, notGo) => {
                x = unitBlock.offsetLeft
                unitAsset.classList.remove(notGo, 'top', 'bottom')
                unitAsset.classList.add(go)
            }
            let goY = (go, notGo) => {
                y = unitBlock.offsetTop
                unitAsset.classList.remove('left', 'right', notGo)
                unitAsset.classList.add(go)
            }
            let watchDirection = setInterval(() => {
                if (x > unitBlock.offsetLeft) { // двигается на право/лево
                    goX('left', 'right')
                } else if (x < unitBlock.offsetLeft) {
                    goX('right', 'left')
                }

                if (y < unitBlock.offsetTop) { // двигается на верх/низ
                    goY('bottom','top')
                } else if (y > unitBlock.offsetTop) {
                    goY('top','bottom')
                }
                if (unitBlock.classList.contains('stay')) {
                    unitAsset.classList.remove('right', 'right', 'top', 'bottom')
                    unitAsset.classList.add('wait')
                } else {
                    unitAsset.classList.remove('wait')
                }
                if (x == -999) {
                    clearInterval(watchDirection)
                }
            },100)

        }
        direction()
    }
    
}

export default animation