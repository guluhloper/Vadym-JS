
class Observer {
    constructor(){
        this.observers = []
    }
    subscribe(observer){
        this.observers.push(observer)
    }
    unsubscribe(observer){
        this.observers = this.observers.filter(subscriber => subscriber != observer)
    }
    broadcast(data){
        this.observers.forEach(subscriber => subscriber(data))
    }
}

export default Observer

