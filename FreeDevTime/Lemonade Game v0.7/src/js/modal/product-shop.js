import _Store from '../data-store';

let shop = {
    prices: {lemons: 0, water: 0, sugar: 0},
    updatePrices: () => {
        shop.prices.lemons = Math.floor(Math.random() * (40 - 20)) + 20; // 1kg
        shop.prices.water = 1; // 1l
        shop.prices.sugar = Math.floor(Math.random() * (1.6 - 1.2)) + 1.2; // 100g
    },
    price: {
        lemons: () => {
            return Math.floor((shop.prices.lemons / 1000) * (Math.random() * (160 - 90) + 90)); // get one random lemon
        },
        water:  () => {
            return 1;
        },
        sugar:  () => {
            return shop.prices.sugar
        }
    },
    renderPrice: () => {
        document.querySelector('.modal .shop .buy-1 .price').innerHTML = `${shop.prices.water} гр/литр`
        document.querySelector('.modal .shop .buy-2 .price').innerHTML = `${shop.prices.lemons} гр/кг`
        document.querySelector('.modal .shop .buy-2 .vight').innerHTML = `1 шт. - ${shop.price.lemons()} гр.`
        document.querySelector('.modal .shop .buy-3 .price').innerHTML = `${shop.prices.sugar} гр/100г`

        document.querySelector('.modal .shop .buy-1 .fas').addEventListener('click', () => {
            buyProduct("water", 1)
        })
        document.querySelector('.modal .shop .buy-2 .fas').addEventListener('click', () => {
            document.querySelector('.modal .shop .buy-2 .vight').innerHTML = `1 шт. - ${shop.price.lemons()} гр.`
            buyProduct("lemons", 1)
        })
        document.querySelector('.modal .shop .buy-3 .fas').addEventListener('click', () => {
            buyProduct("sugar", 100)
        })
    }
}

let buyProduct = (type, q) => {
    let price = Math.floor(shop.price[type]());
    _Store.addComponent(type, price, q)
}


shop.updatePrices()  // обновили цены на продукты на этот день
shop.renderPrice()   // вывели цены

export default shop;

// 15 18 - 480*576