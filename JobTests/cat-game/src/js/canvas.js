let ctx = document.getElementById('canv-2').getContext('2d')

let cW = 500
let cH = 500

let border = {
    x: 1,
    y: 1
}
let cat = {
    x: 200,
    y: 200,
    elW: 100,
    elH: 100,
    step: 100
}

let step = 100

let play = false
let recording = false

let newRecord = []

let catImg = new Image()
catImg.src = "./images/cat.png"
catImg.onload = function() {
    ctx.drawImage(catImg, cat.x, cat.y, cat.elW, cat.elH)  
}

let drawCat = () => {
    ctx.beginPath()
    ctx.rect(cat.x, cat.y, cat.elW, cat.elH)
    ctx.strokeStyle = '#9e9e9e85'
    ctx.stroke()

    drawImg()

    up.disabled = (cat.y < step) ? true : false
    down.disabled = (cat.y >= cH - cat.elH) ? true : false
    left.disabled = (cat.x < step) ? true : false
    right.disabled = (cat.x >= cW - cat.elW) ? true : false  
}

let drawPath = (lastX, LastY, text) => {
    ctx.beginPath()

    ctx.rect(lastX, LastY, cat.elW, cat.elH)
    ctx.fillStyle = '#fff';
    ctx.fill()

    ctx.strokeStyle = '#333';
    ctx.stroke()

    ctx.fillStyle = '#666';
    ctx.fillText(text, lastX + 50, LastY + 50)
}

let drawBorder = () => {
    ctx.beginPath()
    ctx.rect(border.x*100, border.y*100, cat.elW*20, cat.elH*20)
    ctx.strokeStyle = '#9e9e9e85'
    ctx.stroke()
}

let drawImg = () => {
    ctx.drawImage(catImg, cat.x, cat.y, cat.elW, cat.elH)
}

let record = () => {
    let name = 'cat'
    let x = cat.x
    let y = cat.y
    let data = {x, y}
    newRecord.push(data)
    localStorage.setItem(name, JSON.stringify(newRecord))    
}

let reset = () => {
    cat.x = 200
    cat.y = 200
    ctx.clearRect(0, 0, cW, cH)
    drawCat()
}

let moveCat = direction => { 

    if      (direction === 'up') { 
        // border.y--
        cat.y = cat.y - step
    } 
    else if (direction === 'down'){
        // border.y++
        cat.y = cat.y + step
    }
    else if (direction === 'left'){
        // border.x--
        cat.x = cat.x - step
    }
    else if (direction === 'right'){
        // border.x++
        cat.x = cat.x + step
    }

    // if (direction === 'up') { 
    //     if (border.y*step < (step*2)) {
    //         border.y++         
    //     }
    // } 
    // else if (direction === 'down'){
    //     if (border.y*step > (cat.elW*20 - step*37)) {
    //         border.y--          
    //     }
    // }
    // else if (direction === 'left'){
    //     if (border.x*step < (step*2)) {
    //         border.x++         
    //     }
    // }
    // else if (direction === 'right'){
    //     if (border.x*step > (cat.elW*20 - step*37)) {
    //         border.x--          
    //     }
    // }
    // console.log(border.x*step, border.y*step);

    ctx.clearRect(0, 0, cW, cH)
    drawCat()
    // drawBorder()
    
    if (recording == true) { record() }
}

up.addEventListener('click', () =>    {moveCat('up')})
down.addEventListener('click', () =>  {moveCat('down')})
left.addEventListener('click', () =>  {moveCat('left')})
right.addEventListener('click', () => {moveCat('right')})

resetBtn.addEventListener('click', () => {
    reset()
})

playStopBtn.addEventListener('click', () => {
    ctx.clearRect(0, 0, cW, cH)
    recordBtn.disabled = true
    play = !play

    if(play == true) {
        playStopBtn.textContent = 'Stop'
        let recordedPath = JSON.parse(localStorage.getItem('cat'))

        let i = 0
        let p = 0
        setTimeout(function runRecord(){
            if (i == recordedPath.length) {
                play = !play
                playStopBtn.textContent = 'Play'
                recordBtn.disabled = false
            } else if (i >= 0) {
                ctx.clearRect(0, 0, cW, cH)
                cat.x = recordedPath[i].x
                cat.y = recordedPath[i].y
                drawCat()
                // drawBorder()
    
                if (i >= 1){
                    for (let n = 0; n <= p; n++) {
                        lastX = recordedPath[n].x;
                        LastY = recordedPath[n].y;
                        drawPath(lastX, LastY, n+1);
                    }
                    p++
                }
                i++
                setTimeout(runRecord, 500)
            }
        }, 500)
    }
})

recordBtn.addEventListener('click', ()=>{
    recording = !recording
    recordBtn.style.color = (recording == true) ? '#e91e63' : '#000';    
    playStopBtn.disabled  = (recording == true) ? true : false;
    if (recording == true) { record() }
})

drawCat()
// drawBorder()