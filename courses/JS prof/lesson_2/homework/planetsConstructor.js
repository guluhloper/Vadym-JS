/*

  Задание:

  Написать функцию конструктор, которая будет иметь приватные и публичные свойства.
  Публичные методы должны вызывать приватные.

  Рассмотрим на примере планеты:

    - На вход принимаются параметр Имя планеты.

    Создается новый обьект, который имеет публичные методы и свойства:
    - name (передается через конструктор)
    - population ( randomPopulation());
    - rotatePlanet(){
      let randomNumber = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
      if ( (randomNumber % 2) == 0) {
        growPopulation();
      } else {
        Cataclysm();
      }
    }

    Приватное свойство:
    populationMultiplyRate - случайное число от 1 до 10;

    Приватные методы
    randomPopulation -> Возвращает случайное целое число от 1.000 до 10.000
    growPopulation() {
      функция которая берет приватное свойство populationMultiplyRate
      которое равняется случайному числу от 1 до 10 и умножает его на 100.
      Дальше, число которое вышло добавляет к популяции и выводит в консоль сообщение,
      что за один цикл прибавилось столько населения на планете .
    }
    Cataclysm(){
      Рандомим число от 1 до 10 и умножаем его на 10000;
      То число которое получили, отнимаем от популяции.
      В консоль выводим сообщение - от катаклизма погибло Х человек на планете.
    }

*/

function Planet (name) {
  this.name = name;
  this.population = randomPopulation();
  this.populationMultiplyRate = Math.floor(Math.random()*(10-1)+1);

  function randomPopulation() {
    return Math.floor(Math.random()*(10000-1000)+1000);
  };

  let growPopulation = function(){
    this.population += (randomPopulation()*100)
    console.log(`За один цикл оборота земли население выросло до ${this.population}`);
  }.bind(this); // провильно так?

  let Cataclysm = function(){
    this.population -= (this.populationMultiplyRate * 1000);
    this.population = (this.population < 0) ? 0 : this.population;
    console.log(`За один цикл оборота случился катаклизи и население уменьшилось до ${this.population}`)
  };

//почему прототип понимает где брать фн growPopulation(), только находясь в нутри функции конструктора?
  Planet.prototype.rotatePlanet = function() {
    let randomNumber = Math.floor(Math.random() * (1000 - 1)) + 1;
    if ( (randomNumber % 2) == 0) {
      growPopulation();
    } else {
      Cataclysm.call(this); // или правильно так?
    }
  };
}



// Planet.rotatePlanet().bind(this, Planet);

let mars = new Planet('Mars');
console.log(mars);

mars.rotatePlanet(); // не могу понять как я потерял контекст
console.log(mars);
