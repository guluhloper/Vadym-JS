import style from "./index.scss";
import game from "./js/core";

// Run game
addEventListener('DOMContentLoaded', ()  => game.run());