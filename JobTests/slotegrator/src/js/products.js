const coolImages = require("cool-images")

let imagesArr = coolImages.many(210, 210, 5)
let products = document.querySelector('.products')
products.innerHTML = ''

for (let i = 0; i < imagesArr.length; i++) {
    let el = imagesArr[i]
    let card = 
    `
    <div class="card">
        <a href="#">
            <img src="${el}" width="210" height="210" alt="Крутой чайник">
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Itaque, repellat.</p>
            <div class="price">1 900 р.</div>
            <div id="addToCart" class="add-to-cart icon"></div>
        </a>
    </div>
    `
    products.insertAdjacentHTML('beforeend', card)
}

