/*

  Задание - используя классы и (или) прототипы создать программу, которая будет
  распределять животных по зоопарку.

  Zoo ={
    name: '',
    AnimalCount: 152,
    zones: {
      mammals: [],
      birds: [],
      fishes: [],
      reptile: [],
      others: []
    },
    addAnimal: function(animalObj){
      // Добавляет животное в зоопарк в нужную зону.
      // зона берется из класса который наследует Animal
      // если у животного нету свойства zone - то добавлять в others
    },
    removeAnimal: function('animalName'){
      // удаляет животное из зоопарка
      // поиск по имени
    },
    getAnimal: function(type, value){
      // выводит информацию о животном
      // поиск по имени или типу где type = name/type
      // а value значение выбраного типа поиска
    },
    countAnimals: function(){
      // функция считает кол-во всех животных во всех зонах
      // и выводит в консоль результат
    }
  }

  Есть родительский класс Animal у которого есть методы и свойства:
  Animal {
    name: 'Rex', // имя животного для поиска
    phrase: 'woof!',
    foodType: 'herbivore' | 'carnivore', // Травоядное или Плотоядное животное
    eatSomething: function(){ ... }
  }

  Дальше будут классы, которые расширяют класс Animal - это классы:
  - mammals
  - birds
  - fishes
  - pertile

  каждый из них имеет свои свойства и методы:
  в данном примере уникальными будут run/speed. У птиц будут методы fly & speed и т.д
  Mammals = {
    zone: mamal, // дублирует название зоны, ставиться по умолчанию
    type: 'wolf', // что за животное
    run: function(){
      console.log( wolf Rex run with speed 15 km/h );
    },
    speed: 15
  }

  Тестирование:
    new Zoo('name');
    var Rex = new Mammal('Rex', 'woof', 'herbivore', 15 );
    your_zooName.addAnimal(Rex);
      // Добавит в your_zooName.zones.mamals новое животное.
    var Dex = new Mammal('Dex', 'woof', 'herbivore', 11 );
    your_zooName.addAnimal(Dex);
      // Добавит в your_zooName.zones.mamals еще одно новое животное.

    your_zooName.get('name', 'Rex'); -> {name:"Rex", type: 'wolf' ...}
    your_zooName.get('type', 'wolf'); -> [{RexObj},{DexObj}];

    Программу можно расширить и сделать в виде мини игры с интерфейсом и сдать
    как курсовую работу!
    Идеи:
    - Добавить ранжирование на травоядных и хищников
    - добавив какую-то функцию которая иммитирует жизнь в зоопарке. Питание, движение, сон животных и т.д
    - Условия: Если хищник и травоядный попадает в одну зону, то хищник сьедает травоядное и оно удаляется из зоопарка.
    - Графическая оболочка под программу.
*/

class Zoo {
  constructor(name){
    this.name = name;
    this.AnimalCount = 0;
    this.zones = {
      mammals: [],
      birds: [],
      fishes: [],
      reptile: [],
      others: []
    }
  }
  addAnimal(animalObj){
    this.AnimalCount += 1;
    if(this.zones[animalObj.zone] !== undefined) {
      this.zones[animalObj.zone].push(animalObj);
      console.log(`Animal ${animalObj.name} added to zone ${animalObj.zone}`)
    } else {
      this.zones.others.push(animalObj);
      console.log(`Animal ${animalObj.name} add to zone others`)
    }
  }
  removeAnimal(name){
    for (let zone in this.zones) {
      this.zones[zone] = this.zones[zone].filter(el => el.name !== name);     
    }
  }

  getAnimal(type, value){
    let animal;
    for (let zone in this.zones) {
      let animalSearch = this.zones[zone].find(el => el[type] == value);
      animal = (animalSearch !== undefined) ? animalSearch : animal;
    }
    let info = `
      Search by ${type}: ${value}.
      Result: ${animal.name} - ${animal.type} say ${animal.phrase}
    `;
    console.log(info)
  }
  countAnimals(){
    let animalsArr = [];
    for (let zone in this.zones) {
      animalsArr.push(...this.zones[zone]);
    }
    console.log(`Animal Count: ${animalsArr.length}`);
  }
}

class Animal {
  constructor(name, phrase, foodType){
    this.name = name;
    this.phrase = phrase;
    this.foodType = foodType;
    console.log(`Animal created: ${this.name} say ${this.phrase}`)
  }
  eatSomething(){
    console.log(`Animal ${this.name} eat only ${this.foodType}`)
  }
}
class Mammal extends Animal {
  constructor(name, type, phrase, foodType, parm) {
    super(name, phrase, foodType);
    this.zone = 'mammals';
    this.type =  type;
    this.speed = parm;
  }
  run(){
    console.log( `${this.type} ${this.name} run with speed ${this.speed} km/h` );
  };
}
class Bird extends Animal {
  constructor(name, type, phrase, foodType, parm) {
    super(name, phrase, foodType);
    this.zone = 'birds';
    this.type =  type;
    this.speed = parm;
  }
  fly(){
    console.log( `${this.type} ${this.name} fly with speed ${this.speed} km/h` );
  };
}
class Fish extends Animal {
  constructor(name, type, phrase, foodType, parm) {
    super(name, phrase, foodType);
    this.zone = 'fishes';
    this.type =  type;
    this.speed = parm;
  }
  swimming(){
    console.log( `${this.type} ${this.name} swimm with speed ${this.speed} km/h` );
  };
}
class Reptile extends Animal {
  constructor(name, type, phrase, foodType, parm) {
    super(name, phrase, foodType);
    this.zone = 'reptile';
    this.type =  type;
    this.speed = parm;
  }
  run(){
    console.log( `${this.type} ${this.name} run with speed ${this.speed} km/h` );
  };
}
class Bug extends Animal {
  constructor(name, type, phrase, foodType, parm) {
    super(name, phrase, foodType);
    this.zone = 'bugs';
    this.type =  type;
    this.speed = parm;
  }
  run(){
    console.log( `${this.type} ${this.name} run with speed ${this.speed} km/h` );
  };
}
class Others extends Animal {
  constructor(name, type, phrase, foodType, parm) {
    super(name, phrase, foodType);
    this.zone = 'others';
    this.type =  type;
    this.action = parm;
  }
  do(){
    console.log( `${this.type} ${this.name} do ${this.parm}` );
  };
}

//
// Тестировка
console.log('Testing run...')
let ZooTopia = new Zoo('ZooTopia'); // создали парк
console.log(ZooTopia);

let Rex = new Mammal('Rex', 'wolf', 'woof', 'herbivore', 30 ); //Создали животное
Rex.run(); // бежит
let Feen = new Bird('Feen', 'parrot', 'Hello!', 'fruits', 40 );
let Pier = new Bug('Pier', 'bug', 'nothing', 'flowers', 1);

ZooTopia.addAnimal(Rex); // Добавили животное
ZooTopia.addAnimal(Feen);
ZooTopia.addAnimal(Pier);

console.log(ZooTopia.zones);
ZooTopia.countAnimals() // Посчитали количество

ZooTopia.removeAnimal("Rex"); // Удалили "Rex"
console.log(ZooTopia.zones);
ZooTopia.countAnimals() // Посчитали снова

ZooTopia.getAnimal('name','Feen') // Показали информацию о животном по имени "Feen"


