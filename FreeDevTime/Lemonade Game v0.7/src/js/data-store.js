const _data = {
    money: 0,
    lemons: 0,
    water: 0,
    sugar: 0,
    id: 100,
    shelf: [],
    message: [],
}

const Store = {
    setStore: player => {
        let {money, lemons, water, sugar} = player;
        _data.money  += money;
        _data.lemons += lemons;
        _data.water  += water;
        _data.sugar  += sugar;
    },
    getStore: () => {
        let {shelf, money, lemons, water, sugar} = _data;
        return {shelf, money, lemons, water, sugar}
    },
    addComponent: (type, price, q) => {
        _data[type] = (_data[type]+q)
        _data.money -= price; 
    },
    addLemonade: el => {
        let {lemons, water, sugar} = el;
            if (lemons <= _data.lemons && water <= _data.water && sugar <= _data.sugar) {
                _data.id++
                _data.lemons -= lemons;
                _data.water -= water;
                _data.sugar -= sugar;
                if (_data.shelf.length < 5) {
                    _data.shelf.push(Object.assign({id:_data.id},el))
                } else {
                    _data.shelf.shift();
                    _data.shelf.push(Object.assign({id:_data.id},el))
                }
                
            } else {
                _data.message = 'Need more components';
            }
    },
    sellLemonade: (price) => {
        _data.money += price;
    },
    lastSelectedLemonade: (id) => {
        let selected = _data.shelf.find(el => el.id == id)
        _data.shelf = [selected]
    },
    clearShelf: () => { 
        _data.shelf = [] 
    },
    storeLog: (msg) => { 
        _data.message.unshift(msg)
    },
    getLog: () => { return _data.message }
    }

Object.freeze(Store);

export default Store;