import Store from "../application/singleton/new-singleton";

/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [
        {
          id: 0,
          text: '123123'
        }
      ],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/

const _data = {
  laws: [],
  budget: 1000000,
  citizensSatisfactions: 0,
}

const Government = {
  addLaw: (id, name, description) => {
    _data.laws.push({id, name, description});
    _data.citizensSatisfactions -= 10;
    return `Id: ${id}, Name: ${name}, Desc: ${description}`;
  },
  showLaws: () => _data.laws,
  findLaw: id => _data.laws.find( d => d.id === id ),
  showLove: () => _data.citizensSatisfactions,
  showBudget: () => _data.budget,
  runIvent: () => {
    _data.budget -= 50000;
    _data.citizensSatisfactions += 5;
    return 'Провели праздник'
  }
}

Object.freeze(Government);

export default Government;
