/*
  Задание:
  1. При помощи методов изложеных в arrays.js , переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов.
  2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
      + Бонусный бал. Вывести на страничку списком
  3. Реализация функции поиска по массиву ITEA_COURSES.
      + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.

*/

const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];
let main = document.getElementById('main');
let mapList = document.getElementById('map_list');

let arr0 = ITEA_COURSES.map((item, i) =>{
return item.length;
});
console.log('Item lenght: ' + arr0);

let arr1 = ITEA_COURSES.slice().sort();
console.log('Sort: ' + arr1);

let arr2 = arr1.map((item, i) => {
    mapList.innerHTML += `<li>${item}</li>`;
})
console.log('List on page');


let findEl = document.getElementById('find_el');
let findInput = document.createElement('input');
let findBtn = document.createElement('button');
let findResult = document.createElement('ul');
findBtn.innerHTML = 'Find';
findEl.appendChild(findInput);
findEl.appendChild(findBtn);
findEl.appendChild(findResult);

let finding = (el, i, arr) => {
    return el == findInput.value;
}

findBtn.addEventListener('click', ()=>{
    findResult.innerHTML = `<li>${ITEA_COURSES.find(finding)}</li>`;
    console.log(findInput.value)
    findInput.value = "";
})