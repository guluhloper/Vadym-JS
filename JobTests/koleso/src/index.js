import "./sass/style.scss";

import "./js/canvas"
import { updateSettings, loadSettings, loadWheel, runWheel } from "./js/canvas"

loadWheel()

start.addEventListener('click', ()=>{
    updateSettings()
    runWheel()
})

