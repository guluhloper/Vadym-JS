import { popup } from './popup'
export { updateSettings, loadSettings, loadWheel, runWheel }

let ctx = document.getElementById('canv1').getContext('2d')

let cW = canv1.width
let cH = canv1.height
let radius = canv1.width/2
let parts = 6
let partRad = Math.PI*2/parts
let startRad = 0
let winParts = []
let winNum = 0
let time = +new Date
let endTime = 3
let activeTime

let speed = 17
let active = false

// Рандомные цвета для колеса
let colors = []
let getRandomColor = () => {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    colors.push(color)
}

// Долька колеса
let circlePart = (centerX, centerY, radius, part, startRad, color) => {
    let start = startRad + partRad * part
    ctx.beginPath()
    ctx.moveTo(centerX, centerY)
    ctx.arc(centerX, centerY, radius, start, start+partRad)
    ctx.closePath()
    ctx.fillStyle = color;
    ctx.fill();
    ctx.lineWidth = 1;
    ctx.strokeStyle = '#000';
    ctx.stroke();    
}

// Текст в колесе
let drawText = (centerX, centerY, radius, part, startRad, text) => {
    ctx.save();
    ctx.translate(centerX, centerY);
    ctx.rotate(partRad*(part+0.5)+startRad);
    ctx.textAlign = "right";
    ctx.fillStyle = "#000";
    ctx.font = 'bold 20px sans-serif';
    ctx.fillText(text, radius*0.8, parts);
    ctx.restore();
}

// Вращаем колесо, 
let rotateEl = () => {
    // Вращаем до радианы Pi*2
    startRad += speed/360.123
    if (startRad >= Math.PI*2) { startRad = 0 }

    ctx.clearRect(0, 0, cW, cH)

    for (let i = 0; i < parts; i++) {
        circlePart(cW/2, cH/2, radius, i, startRad, colors[i])
        drawText(cW/2, cH/2, radius, i, startRad, i+1)
    }
    
    // Останавливаем когда приходит время
    time = +new Date
    active  = (time >= activeTime) ? false : true

    if (active == true) {
        window.requestAnimationFrame(rotateEl)
        start.disabled = true
    } else {
        start.disabled = false
        // Полученное значение радианы ищем в заранее созданом массиве отрезков радиан
        for (let i = 0; i < winParts.length; i++) {
            const el = winParts[i];
            if (startRad > el.start && startRad < el.end) {
                // Получаем число с колеса
                winNum = el.num
                // console.log('win rad', startRad);
                // console.log('win part', el);
                // console.log(el.num);
            }
        }
        //Отправляем выпавшее число в модельное окно
        popup(winNum)
    }
}

// Задаём переменные параметры: количество частей, время вращения, скорость.
let loadSettings = (pices, rotatingTime, rotatingSpeed) => {
    parts = pices
    endTime = rotatingTime
    speed = rotatingSpeed
    partRad = Math.PI*2/parts

    // Рандомим цвета, создаём массив отрезком круга.
    for (let i = 1; i <= parts; i++) {
        getRandomColor()
        winParts.push({   
            num: i,
            start: Math.PI*2 - (partRad*(i)),
            end: Math.PI*2 - (partRad*(i-1))
        })
    }
}

// Рисуем колесо
let loadWheel = () => {
    startRad = 0
    for (let i = 0; i < parts; i++) {
        getRandomColor()
        circlePart(cW/2, cH/2, radius, i, startRad, colors[i])
        drawText(cW/2, cH/2, radius, i, startRad, i+1)
    }    
}

// Крутим колесо до слегка рандомного времени
let runWheel = () => {
    time = +new Date
    activeTime = time + (Math.random()*(endTime*1000*1.5 - (endTime*1000*0.7)) + (endTime*1000)*0.7);
    active = !active
    rotateEl() 
    // console.log(activeTime - time);
    
}

// Подтягиваем значения с инпутов в разделе 'Settings'.
let controls = document.querySelector('.controls')
let inputs = controls.querySelectorAll('input')
let updateSettings = () => {
    let settings = {
        p: 0,
        t: 0,
        s: 0
    }
    inputs.forEach(el => {
        if (el.name === 'parts') {
            settings.p = el.value
        } else if (el.name === 'time') {
            settings.t = el.value
        } else if (el.name === 'speed') {
            settings.s = el.value
        }
    })
    loadSettings(settings.p, settings.t, settings.s)
}

