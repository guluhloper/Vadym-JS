
let modal = {
    data: {
        price: 10,
        selectedLemonade: {}
    },
    modalBlock: document.querySelector('.modal'),
    modalNoOff(selector){ // показать модалку или скрыть все модалки
        document.querySelector(`.${selector}`).classList.toggle('to-main');
        this.modalBlock.classList.toggle('modal_bg');
        if (this.modalBlock.getElementsByClassName('to-main').length !== 0) {
            this.modalBlock.querySelector('.close_modal').addEventListener('click', () => {
                document.querySelector(`.${selector}`).classList.remove('to-main');
                this.modalBlock.classList.remove('modal_bg');
            })
        }
        let menuBtns = document.querySelectorAll('.circle li .placeholder a') // скрытие меню
        menuBtns.forEach(element => {
            element.addEventListener('click', () => {
                document.getElementById('menu__active').checked = false
            })
        })
    },
    // Окно нового игрока
    newForm(){
        this.modalNoOff('new-game')
        let form = this.modalBlock.querySelector('.new-game');
        form.innerHTML = `
        <form class="new-player-form" id="newPlayerForm" action="" onsubmit="return false">
            <h2>Новый игрок</h2>
            <div class="usen-name-block">
                <input type="text" id="user_name" name="name" class="Input-text" placeholder="Имя"><br>
            </div>
            <div class="dif inputGroup">     
                <input type="radio" value="easy" name="dif" id="easy" checked />
                <input type="radio" value="hard" name="dif" id="hard"/>
                <div class="switch">
                    <label for="easy">ПОЛЕГЧЕ</label>
                    <label for="hard">ПОСЛОЖНЕЕ</label>
                    <span></span>
                </div>
                <div class="dif-info" for="easy">
                Вы решили открыть свой первый бизнес. Вы никогда не занимались чем либо подобным, но что может быть проще лимонада? Разве нет?
                Родители купили вам всё что надо. Это будет просто!
                </div>
                <div class="dif-info" for="hard">
                Вы долго не могли найти работу. На последние деньги вам продали телегу и разный хлам. Возможно это ваш последний шанс. Придётся постараться!
                </div>
            </div>
            <input class="submit-form" type="submit" id="submitForm">
        </form>
        `
    },

    user(){
        let block = document.querySelector('.user')
    },
    // Окно создания лимонада
    recipes(){
        let block = document.querySelector('.recipes');
        for (let i = 0; i < 3; i++) {
            let img = i==0 ? 'src/img/icons/water.png' : i==1 ? 'src/img/icons/lemon.png' : 'src/img/icons/sugar.png';
            let units = i==0 ? 'л' : i==1 ? 'шт' : 'гр';
            let minmax = i==0 ? 20 : i==1 ? 100 : 999;            
            block.insertAdjacentHTML('beforeend', `
            <div class="slider">
                <img class="el${i+1}" src="${img}">
                <div class="range-slider">
                    <span class="rs-label num-${i}">0</span>
                    <input class="rs-range num-${i}" type="range" value="0" min="0" max="${minmax}">
                </div>
                <div class="box-minmax">
                    <span>0</span><span>200</span>
                </div>
                <style>
                .modal .recipes .rs-label.num-${i}::after {
                    content: "${units}";
                }
                </style>
            </div>
            `);
            let rangeSlider = document.querySelector(`.rs-range.num-${i}`);
            let rangeBullet = document.querySelector(`.rs-label.num-${i}`);

            rangeSlider.addEventListener("input", showSliderValue, false);

            function showSliderValue() {
                rangeBullet.innerHTML = rangeSlider.value;
                let bulletPosition = (rangeSlider.value /rangeSlider.max);
                rangeBullet.style.left = (bulletPosition * 225) + "px";
            }
        }
        block.insertAdjacentHTML('beforeend',`
        <button class="btn-3 create_lemonade ">Сделать лимонад</button>
        <div class="recipes_history"></div>
        `);
        document.querySelector('.lemicon').addEventListener('click', () => {
            this.modalNoOff('recipes')
        });
    },
    shop(){
        let block = document.querySelector('.shop')
        document.querySelector('.main-menu .open_shop').addEventListener('click', () => {
            this.modalNoOff('shop')
        })
        let shop_content = `
        <div class="buy-1">
            <img src="src/img/icons/water.png">
            <p class="price">10</p>
            <i class="fas fa-plus-square"></i>
        </div>
        <div class="buy-2">
            <img src="src/img/icons/lemon.png">
            <p class="vight">100</p>
            <p class="price">10</p>
            <i class="fas fa-plus-square"></i>
        </div>
        <div class="buy-3">
            <img src="src/img/icons/sugar.png">
            <p class="price">10</p>
            <i class="fas fa-plus-square"></i>
        </div>
        `
        block.innerHTML = shop_content;
    },
    // Окно начала дня
    dayStart(){
        let block = document.querySelector('.day-start')
        document.querySelector('.day_start_btn').addEventListener('click', () => {
            this.modalNoOff('day-start')
            block.querySelector('.start_day').classList.add('disabled')
            
            block.querySelector('.select_lemonade').addEventListener('click', () => {
                let radio = document.getElementsByName('lemonadeSelect')
                for (let i = 0; i < radio.length; i++) {
                    radio[i].classList.remove('selected')
                    if (radio[i].checked) {
                        radio[i].classList.add('selected')
                        block.querySelector('.start_day').classList.remove('disabled')
                    }                  
                }
            })            
        })

        let price = block.querySelector('.num')
        block.querySelector('.minus').addEventListener('click', () => {
            price.textContent = (Number(price.textContent) > 0 ? Number(price.textContent) - 1 : 0)
            modal.recipes = price.textContent
        })
        block.querySelector('.plus').addEventListener('click', () => {
            price.textContent = Number(price.textContent) + 1
            modal.recipes = price.textContent
        })
    },
    dayEnd(){
        let block = document.querySelector('.day-end')
        return block;
    }
}

modal.newForm()
modal.recipes()
modal.shop()
modal.dayStart()
modal.dayEnd()

export default modal;
