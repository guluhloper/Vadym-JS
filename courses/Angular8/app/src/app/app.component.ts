import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay } from 'rxjs/operators';
import { Todo, TodosService } from "./todos.service";
import { Logs } from 'selenium-webdriver';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: []
})
export class AppComponent implements OnInit {

  todos: Todo[] = []
  todoTitle: string = ""
  loading = false
  error = ''

  constructor(private todoService: TodosService){}

  ngOnInit(){
    this.fetchTodo()
  }

  addTodo(){
    if(!this.todoTitle.trim()){
      return
    }
    this.todoService.addTodo({
      title: this.todoTitle,
      completed: false
    }).subscribe(todo => {
        this.todos.unshift(todo)
        this.todoTitle = ''
      })
  }

  fetchTodo(){
    this.loading = true
    this.todoService.fetchTodos()
    .subscribe(todos => {
      this.todos = todos
      this.loading = false
    }, error => {
        this.error = error.message
        
    })
  }
  removeTodo(id: number){
    this.todoService.removeTodo(id)
    .subscribe(() => {
      this.todos =  this.todos.filter(el => el.id !== id)
    })
  }
  completeTodo(id: number){
    this.todoService.completeTodo(id).subscribe(todo => {
      // console.log(todo);
      
      this.todos.find(el => el.id === todo.id).completed = true
      
    })
  }
}
