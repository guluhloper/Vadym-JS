
import style from "./index.scss"
// import { isatty } from "tty";
// import { on } from "cluster";

let data;
let loadJson = document.getElementById('doIt');
let simplefy = document.getElementById('doThat');
let filterActive = document.getElementById('doAndThis');
let removeCopies = document.getElementById('doMore');

// Вывод длины массива, вывод массива в консоль.
let arrLenght = (respond) => {
    document.querySelector('.respond').innerHTML = 'Длина массива: ' + respond.length
    console.log('Массив: ', respond)
};

// Перебор массива и его обновление
let loopArr = (arr, nextAction) => {
    for (let i = 0; i < arr.length; i++) {
        const obj = arr[i];
        arr = nextAction(arr, obj, i);
    }
    return arr;
}

// Ставим массив за родителем и удаляем его из родителя.
let moveArrUp = (arr, obj, i) => {
    if(obj.children !== undefined && obj.children.length > 0){
        const chArr = obj.children;
        obj.children = [];
        arr.splice((i+1), 0, chArr);
        arr = arr.flat();
        return arr;
    } else {
        return arr;
    }
}
// Фильтруем массивы по параметру isActive
let filterLogic = (arr, obj, i) => {
    if(obj.children !== undefined && obj.children.length > 0){
        let childArr = obj.children.filter(obj => obj.isActive === true);
        obj.children = childArr;
        const newArr = arr.filter(obj => obj.isActive === true);
        return newArr
    } else {
        const newArr = arr.filter(obj => obj.isActive === true);
        return newArr
    }
}

// let deepFilter = (obj, objIndex, arr) => {
//     let one = Object.values(obj)
//     let newArr = arr.map((el,i,array) => {
//         let isTrue = []
//         let currentElValues = Object.values(el)
//         if(objIndex !== i){
//             isTrue = one.map(el1 => {
                
//             })
//         }
//     })
// }

// Load JSON, клопка 1
loadJson.addEventListener('click', ()=>{
    fetch(
        'data/data.json', 
        // 'data/short.json', 
        {method: 'GET'}
    )
    .then( response => response.json() )
    .then( json => {
        data = json;
        arrLenght(data);
    } )
    .catch( error => console.error('error:', error) );
})

// Разбираем массив, кнопка 2
simplefy.addEventListener('click', ()=>{
    data = loopArr(data, moveArrUp);
    arrLenght(data);
})

// Фильтруем массив. (Пока только одномерный массив)
filterActive.addEventListener('click', ()=>{
    data = loopArr(data, filterLogic);
    arrLenght(data);
})

// Удаляем дубли. (Быстрое, но не самое универсальное решение)
// План: Надо сделать фильтрацию с перебором массива на 100% ответ includes() для всех (трёх) свойств объекта.
removeCopies.addEventListener('click', ()=> {
    let noCopyesByStringify = data.filter((obj,i,arr) => {
        let arrString = arr.map(el => {
            return JSON.stringify(el);
        })
        let objString = JSON.stringify(obj);
        return arrString.indexOf(objString) == i;
    })
    arrLenght(noCopyesByStringify);
})