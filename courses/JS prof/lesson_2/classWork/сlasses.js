/*
  Задание:

    I. Написать класс Post который будет уметь:

      1. Конструктор:
          title
          image
          description
          likes

      2. Методы
          render -> отрисовать элемент в ленте
          likePost -> увеличивает счетчик постов
          + Методы для изменения title, image, description
          + бонус. Сделать получение и изменение через set и get

    II. Написать класс Advertisment который будет экстендится от класа Post
        но помимо будет иметь другой шаблон вывода, а так же метод для покупки обьекта

        buyItem -> выведет сообщение что вы купили обьект

    III.  Сгенерировать ленту из всех постов что вы добавили в систему.
          Каждый третий пост должен быть рекламным

    <div class="post">
      <div class="post__title">Some post</div>
      <div class="post__image">
        <img src="https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png"/>
      </div>
      <div class="post__description"></div>
      <div class="post__footer">
        <button class="post__like">Like!</button>
      </div>
    </div>




*/

const Posts = [];
const posts_feed = document.getElementById('posts_feed');

document.addEventListener('DOMContentLoaded', () => {
  class Post {
    constructor(title, image, description, id, likes) {
      this.title = title;
      this.image = image;
      this.description = description;
      this.id = Posts.length;
      this.likes = 0;

      this.addLike = this.addLike.bind( this );
      Posts.push( this );  
    }

    render() {
      let post, likeBtn, inDom;
      inDom = document.querySelector(`.post[data-id="${this.id}"]`);

      if(inDom == null) {
        post = document.createElement('div');
        post.classList.add('post')
        post.setAttribute('data-id',`${this.id}`);
      } else {
        post = inDom;
      }
      post.innerHTML = `
        <div class="post__title">${this.title}</div>
        <div class="post__image">
          <img src="${this.image}"/>
        </div>
        <div class="post__description">${this.description}</div>
        <div class="post__footer">
          <button class="post__like">Like! ${this.likes}</button>
        </div>
      `;

      if(inDom == null) {
        posts_feed.appendChild(post);
      }

      likeBtn = post.querySelector('.post__like')
      likeBtn.onclick = this.addLike;
    }
    
    addLike(){
      this.likes++;
      this.render();
    }
  }

  class Advertisment extends Post {
    constructor(title, image, description, id, likes, amount){
      super(title, image, description, id, likes)
      this.amount = 0;
      this.buyIt = this.buyIt.bind(this);
    }

    render() {
      let post, likeBtn, buyBtn, inDom;
      inDom = document.querySelector(`.post[data-id="${this.id}"]`);

      if(inDom == null) {
        post = document.createElement('div');
        post.classList.add('post')
        post.setAttribute('data-id',`${this.id}`);
      } else {
        post = inDom;
      }
      post.innerHTML = `
        <div class="post__title">${this.title}</div>
        <div class="post__description">${this.description}</div>
        <div class="post__image">
          <img src="${this.image}"/>
        </div>
        <div class="post__footer">
          <button class="post__like">Like! ${this.likes}</button>
        </div>
        <div class="buy">
          <button class="buy_it">Купить</button>
        </div>
      `;

      if(inDom == null) {
        posts_feed.appendChild(post);
      }

      likeBtn = post.querySelector('.post__like')
      likeBtn.onclick = this.addLike;

      buyBtn = post.querySelector('.buy_it')
      buyBtn.onclick = this.buyIt;
      if (this.amount > 0) {
        alert(`
        Вы купили: ${this.title}
        Количество: ${this.amount}
        `)
      }
    }
    
    addLike(){
      this.likes++;
      this.render();
    }
    buyIt(){
      this.amount++;
      this.render();
    }
  }

  new Post('Looks nice', 'https://www.natgeokids.com/wp-content/uploads/2017/06/Ocean-facts-7-1.jpg', 'Красивая кратинка моря. Там обязательно надо побывать.');
  new Post('Выглядит красиво', 'https://www.natgeokids.com/wp-content/uploads/2017/06/Ocean-facts-7-1.jpg', 'Красивая кратинка моря. Там обязательно надо побывать.');
  new Advertisment('Смотри как красиво', 'https://ae01.alicdn.com/kf/HTB1fSBNQXXXXXXuXFXXq6xXFXXXD/21-5-14-5-cm-du-y-rozmiar-R-czniki-Papierowe-Pude-ka-bielizna-opakowania-pude.jpg_640x640.jpg', 'Красивая кратинка моря. Там обязательно надо побывать.');
  new Advertisment('Купи это красиво красиво', 'https://ae01.alicdn.com/kf/HTB1fSBNQXXXXXXuXFXXq6xXFXXXD/21-5-14-5-cm-du-y-rozmiar-R-czniki-Papierowe-Pude-ka-bielizna-opakowania-pude.jpg_640x640.jpg', 'Красивая кратинка моря. Там обязательно надо побывать.');


  Posts.map( post => post.render() ); 

});
