/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classwork_observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classwork/observer */ \"./classwork/observer.js\");\n// Точка входа в наше приложение\r\n// import Observer from './observer';\r\n// import HOF from './hoc';\r\n// import CustomEvents from './observer/CustomEvents';\r\n\r\n\r\n// 0. HOC\r\n// HOF();\r\n// 1. Observer ->\r\n// console.log( Observer );\r\n// Observer();\r\n// console.log('INDEX');\r\n// 2. CustomEvents ->\r\n// CustomEvents();\r\nObject(_classwork_observer__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/observer/Observer.js":
/*!******************************************!*\
  !*** ./application/observer/Observer.js ***!
  \******************************************/
/*! exports provided: Observable, Observer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observable\", function() { return Observable; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observer\", function() { return Observer; });\nfunction Observable(){\r\n  // Создаем список подписаных обьектов\r\n  const observers = [];\r\n  // Оповещение всех подписчиков о сообщении\r\n  this.sendMessage = function( msg ){\r\n      observers.map( ( obs ) => {\r\n        obs.notify(msg);\r\n      });\r\n  };\r\n  // Добавим наблюдателя\r\n  this.addObserver = function( observer ){\r\n    observers.push( observer );\r\n  };\r\n}\r\n\r\n// Сам наблюдатель:\r\nfunction Observer( behavior ){\r\n  // Делаем функцию, что бы через callback можно\r\n  // было использовать различные функции внутри\r\n  this.notify = function( msg ){\r\n    behavior( msg );\r\n  };\r\n}\r\n\r\n\r\n\r\n\r\n\n\n//# sourceURL=webpack:///./application/observer/Observer.js?");

/***/ }),

/***/ "./classwork/observer.js":
/*!*******************************!*\
  !*** ./classwork/observer.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _application_observer_Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../application/observer/Observer */ \"./application/observer/Observer.js\");\n/*\r\n  Задание: Модуль создания плейлиста, используя паттерн Обсервер.\r\n\r\n  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:\r\n    1. Список исполнителей и песен (Находится слева) - отуда можно включить\r\n    песню в исполнение иди добавить в плейлист.\r\n    Если песня уже есть в плейлисте, дважды добавить её нельзя.\r\n\r\n    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,\r\n    или запустить в исполнение. Внизу списка должен выводиться блок, в котором\r\n    пишет суммарное время проигрывания всех песен в плейлисте.\r\n\r\n    3. Отображает песню которая проигрывается.\r\n\r\n    4. + Бонус: Сделать прогресс пар того как проигрывается песня\r\n    с возможностью его остановки.\r\n*/\r\n\r\n\r\n\r\nconst MusicList = [\r\n  {\r\n    title: 'Rammstain',\r\n    songs: [\r\n      {\r\n        id: 1,\r\n        name: 'Du Hast',\r\n        time: [3, 12]\r\n      },\r\n      {\r\n        id: 2,\r\n        name: 'Ich Will',\r\n        time: [5, 1]\r\n      },\r\n      {\r\n        id: 3,\r\n        name: 'Mutter',\r\n        time: [4, 15]\r\n      },\r\n      {\r\n        id: 4,\r\n        name: 'Ich tu dir weh',\r\n        time: [5, 13]\r\n      },\r\n      {\r\n        id: 5,\r\n        name: 'Rammstain',\r\n        time: [3, 64]\r\n      }\r\n    ]\r\n  },\r\n  {\r\n    title: 'System of a Down',\r\n    songs: [\r\n      {\r\n        id: 6,\r\n        name: 'Toxicity',\r\n        time: [4, 22]\r\n      },\r\n      {\r\n        id: 7,\r\n        name: 'Sugar',\r\n        time: [2, 44]\r\n      },\r\n      {\r\n        id: 8,\r\n        name: 'Lonely Day',\r\n        time: [3, 19]\r\n      },\r\n      {\r\n        id: 9,\r\n        name: 'Lost in Hollywood',\r\n        time: [5, 9]\r\n      },\r\n      {\r\n        id: 10,\r\n        name: 'Chop Suey!',\r\n        time: [2, 57]\r\n      }\r\n    ]\r\n  },\r\n  {\r\n    title: 'Green Day',\r\n    songs: [\r\n      {\r\n        id: 11,\r\n        name: '21 Guns',\r\n        time: [4, 16]\r\n      },\r\n      {\r\n        id: 12,\r\n        name: 'Boulevard of broken dreams!',\r\n        time: [6, 37]\r\n      },\r\n      {\r\n        id: 13,\r\n        name: 'Basket Case!',\r\n        time: [3, 21]\r\n      },\r\n      {\r\n        id: 14,\r\n        name: 'Know Your Enemy',\r\n        time: [4, 11]\r\n      }\r\n    ]\r\n  },\r\n  {\r\n    title: 'Linkin Park',\r\n    songs: [\r\n      {\r\n        id: 15,\r\n        name: 'Numb',\r\n        time: [3, 11]\r\n      },\r\n      {\r\n        id: 16,\r\n        name: 'New Divide',\r\n        time: [4, 41]\r\n      },\r\n      {\r\n        id: 17,\r\n        name: 'Breaking the Habit',\r\n        time: [4, 1]\r\n      },\r\n      {\r\n        id: 18,\r\n        name: 'Faint',\r\n        time: [3, 29]\r\n      }\r\n    ]\r\n  }\r\n]\r\n\r\nconst MusicBox = () => {\r\n  const MusicBox = document.getElementById('MusicBox');\r\n  MusicList.forEach(element => {\r\n    let artist = `\r\n    <div class=\"artist\">${element.title}</div>\r\n    `;\r\n    let artistSong = element.forEach(elem => {\r\n      let song = `\r\n      <li class=\"song\">${elem.name}</li>\r\n      `\r\n    });\r\n    artist.innerHTML =+ artistSong;\r\n    MusicBox.innerHTML =+ artist;\r\n  });\r\n  // MusicList.map( Artist => {\r\n  //   MusicBox.innerHTML += `<div><h4>${Artist.title}</h4><ul>`;\r\n  //   Artist.songs.map( song => {\r\n  //     MusicBox.innerHTML += `<li id=\"_s${song.id}\">${song.name}</li>`;\r\n  //     let songItem = MusicBox.querySelector(`#_s${song.id}`);\r\n  //     console.log(songItem)\r\n  //     songItem.addEventListener('click', (e) => {\r\n  //       console.log('e', e)\r\n  //     });\r\n  //     // songItem.onclick = () => console.log(123)\r\n  //   });\r\n  //   MusicBox.innerHTML += '</ul></div>';\r\n  // });\r\n}\r\n\r\n\r\n\r\n\r\n    \r\nlet playList = [];\r\nlet nowPlaing;\r\n\r\nlet miObservable = new _application_observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\r\n\r\nlet selectSong = new _application_observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( function(id) {\r\n  nowPlaing = MusicList.filter( i => Number(i.id) === Number(id))\r\n});\r\n\r\n miObservable.addObserver(selectSong); \r\n\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (MusicBox);\r\n\n\n//# sourceURL=webpack:///./classwork/observer.js?");

/***/ })

/******/ });